<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'home','check'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $status = Yii::$app->user->identity->status;

        if($status == 0){
            return $this->redirect(['user/settings/verify']);
        } else {
            if (\Yii::$app->user->can('admin')) {

                return $this->redirect(['/admin/index']);

            } else if (\Yii::$app->user->can('user-treg')) {

                return $this->redirect(['/treg/index']);

            } else if (\Yii::$app->user->can('user-witel')) {

                return $this->redirect(['/witel/index']);

            } else if (\Yii::$app->user->can('user-ubis')) {

                return $this->redirect(['/ubis/index']);

            } else if (\Yii::$app->user->can('salesforce')) {

                return $this->redirect(['/salesforce/index']);

            } 
            else {
                return $this->goBack();
            }
        }
        // return $this->redirect(['check']);
        // return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionHome()
    {
        return $this->goHome();
    }

    public function actionCheck()
    {
        $status = Yii::$app->user->identity->status;

        if($status == 0){
            return $this->redirect(['user/settings/verify']);
        } else {
            if (\Yii::$app->user->can('admin')) {

                return $this->redirect(['/admin/index']);

            } else if (\Yii::$app->user->can('user-treg')) {

                return $this->redirect(['/treg/index']);

            } else if (\Yii::$app->user->can('user-witel')) {

                return $this->redirect(['/witel/index']);

            } else if (\Yii::$app->user->can('user-ubis')) {

                return $this->redirect(['/ubis/index']);

            } 
            else {
                return $this->goBack();
            }
        }

    }
}
