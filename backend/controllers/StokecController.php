<?php

namespace backend\controllers;

use Yii;
use common\models\UbisKecamatan;
use common\models\UbisKecamatanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UbiskecController implements the CRUD actions for UbisKecamatan model.
 */
class UbiskecController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UbisKecamatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = UbisKecamatan::find();       
          
        $searchModel = new UbisKecamatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UbisKecamatan model.
     * @param string $ubis_id
     * @param string $kecamatan_id
     * @return mixed
     */
    public function actionView($ubis_id, $kecamatan_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($ubis_id, $kecamatan_id),
        ]);
    }

    /**
     * Creates a new UbisKecamatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UbisKecamatan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ubis_id' => $model->ubis_id, 'kecamatan_id' => $model->kecamatan_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UbisKecamatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $ubis_id
     * @param string $kecamatan_id
     * @return mixed
     */
    public function actionUpdate($ubis_id, $kecamatan_id)
    {
        $model = $this->findModel($ubis_id, $kecamatan_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ubis_id' => $model->ubis_id, 'kecamatan_id' => $model->kecamatan_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UbisKecamatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $ubis_id
     * @param string $kecamatan_id
     * @return mixed
     */
    public function actionDelete($ubis_id, $kecamatan_id)
    {
        $this->findModel($ubis_id, $kecamatan_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UbisKecamatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $ubis_id
     * @param string $kecamatan_id
     * @return UbisKecamatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ubis_id, $kecamatan_id)
    {
        if (($model = UbisKecamatan::findOne(['ubis_id' => $ubis_id, 'kecamatan_id' => $kecamatan_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
