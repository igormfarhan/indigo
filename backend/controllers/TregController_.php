<?php

namespace backend\controllers;

use Yii;
use common\models\Pelanggan;
use common\models\PelangganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\KotaKabupaten;
use common\models\Kecamatan;
use common\models\DesaKelurahan;
use common\models\UbisKecamatan;
use common\models\UbisKecamatanSearch;
use backend\models\User;
use backend\models\UserSearch;
use yii\web\UploadedFile;
use yii\rbac\ManagerInterface;


/**
 * PelangganController implements the CRUD actions for Pelanggan model.
 */
class TregController extends Controller
{
    public $uploadParam = 'file';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionHome()
    {
        return $this->render('home');
    }

    public function actionDashboard($id)
    {
        $searchModel = new PelangganSearch();  
        
        $subQuery = User::find()->select('id')->where(['ubis_id'=> Yii::$app->user->identity->ubis_id])
        ->andWhere(['>','id',200]);
        
        switch ($id) {
            case 1:
                $query = Pelanggan::find()->where(['salesforce_id'=> $subQuery]);
              break;
            case 2:
                $query = Pelanggan::find()->where(['salesforce_id'=> $subQuery,'status_id'=>101]);
              break;
            case 3:
                $query = Pelanggan::find()->where(['salesforce_id'=> $subQuery,'hasil_id'=>201]);
              break;
            case 4:
                $query = Pelanggan::find()->where(['salesforce_id'=> $subQuery,'hasil_id'=>203]);
              break;
           
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


    }

    /**
     * Lists all Pelanggan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PelangganSearch();

        $role = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
        $ubis_id = Yii::$app->user->identity->ubis_id;

        $query = Pelanggan::find();

        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
          
        ]);
    }

    /**
     * Displays a single Pelanggan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Pelanggan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pelanggan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreate2()
    {
        $model = new Pelanggan();

        if ($model->load(Yii::$app->request->post())) {
            $model->salesforce_id = 3;
            $model->save();

            return $this->render('hello');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pelanggan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pelanggan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pelanggan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pelanggan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pelanggan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionListkec($id){

        $kecamatans = Kecamatan::find()->where(['kota_kabupaten_id' => $id])
                                       ->orderBy('id ASC')
                                       ->all();
        // print_r($kecamatans)
				
		if (!empty($kecamatans)) {
			foreach($kecamatans as $kecamatan) {
				echo "<option value='".$kecamatan->id."'>".$kecamatan->nama."</option>";
			}
		} else {
			echo "<option>-</option>";
		}
    }

    public function actionProfile()
    {
        /** @var SettingsForm $model */
        $model = \Yii::createObject(SettingsForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_ACCOUNT_UPDATE, $event);
        if ($model->load(\Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'avatar');

            if (!is_null($image)) {
                // save with image
                 // ubisre the source file name
                
                $model->avatar = $image->name;
                $tmp = explode(".", $image->name);
                $ext = end($tmp);
                // generate a unique file name to prevent duplicate filenames
                $model->avatar = Yii::$app->user->identity->username. '_avatar'.".{$ext}";
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/avatar/';
                $path = Yii::$app->params['uploadPath'] . $model->avatar;
                $image->saveAs($path);                
            }



            $model->save();
            \Yii::$app->session->setFlash('success', \Yii::t('user', 'Your account details have been updated'));
            $this->trigger(self::EVENT_AFTER_ACCOUNT_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('account', [
            'model' => $model, 
        ]);
    }

    public function actions()
    {
        Yii::$app->params['uploadPath'] =Yii::getAlias('@files') . '\dashboard\uploads\avatar';
        $path = Yii::$app->params['uploadPath'];  
        
        return [
            'avatar' => [
                'class' => 'developit\jcrop\actions\Upload',
                'url' => '/dashboard/uploads/avatar/', // downloaded image
                'path' => '@webroot/uploads/avatar',
                'name' => Yii::$app->user->identity->nik . '_avatar',
            ],
        ];
    }

}
