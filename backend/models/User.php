<?php

namespace backend\models;
use dektrium\user\models\User as BaseUser;
use common\models\Ubis;
use common\models\Datel;
use common\models\Witel;

class User extends BaseUser implements \yii\web\IdentityInterface
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const NEED_APPROVAL = 99;
    const REFUSED = 98;

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }


    /**
     * {@inheritdoc}
     */

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function getWitel()
    {
        return $this->hasOne(Witel::classname(),['id'=>'witel_id'])
                    ->via('datel');
    }

    public function getDatel()
    {
        return $this->hasOne(Datel::classname(),['id'=>'datel_id'])
                    ->via('ubis');
    }

    public function getUbis()
    {
        return $this->hasOne(Ubis::className(), ['id' => 'ubis_id']);
    }

    public function getStatusText()

{

    switch ($this->status) {

    case self::ACTIVE:

        $text = "ACTIVE";

        break;

    case self::INACTIVE:

        $text = "INACTIVE";

        break;
    case self::NEED_APPROVAL:

        $text = "NEED APPROVAL";

        break;

    case self::REFUSED:

        $text = "REFUSED";

        break;

    default:

        $text = "(Undefined)";

        break;

    }

    return $text;

}

}
