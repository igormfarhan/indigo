<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\User;

/**
 * UserSearch represents the model behind the search form of `backend\models\User`.
 */
class UserSearch extends User
{
    public $witel;
    public $ubis;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order', 'ubis_id', 'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'last_login_at', 'status','active_at'], 'integer'],
            [['nik', 'nama', 'no_handphone', 'email', 'password_hash', 'auth_key', 'unconfirmed_email', 'registration_ip', 'avatar'], 'safe'],
            [['witel','ubis'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'ubis_id'              => Yii::t('user', 'Ubis'),
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$query)
    {
        // $query = User::find();

        // $query->joinWith('ubis.datel.witel');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['witel'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['ubis.datel.witel.nama' => SORT_ASC],
            'desc' => ['ubis.datel.witel.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ubis'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['ubis.nama' => SORT_ASC],
            'desc' => ['ubis.nama' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order' => $this->order,
            'ubis_id' => $this->ubis_id,
            'confirmed_at' => $this->confirmed_at,
            'blocked_at' => $this->blocked_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'last_login_at' => $this->last_login_at,
            'status' => $this->status,
            'witel_id' => $this->witel,
            'ubis_id' => $this->ubis,
            'active_at' => $this->active_at

        ]);

        $query->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'no_handphone', $this->no_handphone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'unconfirmed_email', $this->unconfirmed_email])
            ->andFilterWhere(['like', 'registration_ip', $this->registration_ip])
            ->andFilterWhere(['like', 'avatar', $this->avatar]);

        return $dataProvider;
    }
}
