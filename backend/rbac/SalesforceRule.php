<?php 
namespace backend\rbac;

use yii\rbac\Rule;
use common\models\Pelanggan;

/**
 * Checks if authorID matches user passed via params
 */
class SalesforceRule extends Rule
{
    public $name = 'isSalesforce';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        var_dump($params); //cek dengan ini dulu
        die();
        if(isset($params['id'])){
            $id = @$params['id'];
            $pelanggan = Pelanggan::findOne($id);
            return $pelanggan ? $pelanggan->salesforce_id == $user : false;            
        }
    }
}