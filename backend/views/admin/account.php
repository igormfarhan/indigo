<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\SettingsForm $model
 */

$this->title = Yii::t('user', 'Account settings');
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user->identity;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?= Html::img('@web/uploads/avatar/'.Yii::$app->User->identity->avatar, [
                        'class' => 'img-rounded',
                        'alt' => $user->username,
                    ]) ?>
                    <?= $user->username ?>
                </h3>
            </div>
            <div class="panel-body">
                <?= UserMenu::widget() ?>
            </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title text-center">Account detail</h3>
          </div>
              
          <div class="box-body box-profile">
            <?= Html::img('@web/uploads/avatar/'.Yii::$app->User->identity->avatar, 
                ['alt' => 'avatar','class'=>'profile-user-img img-responsive img-circle']) 
            ?>

            <h3 class="profile-username text-center"><?= Yii::$app->User->identity->name ?></h3>
            
            <p class="text-muted text-center">Software Engineer</p>

            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Followers</b> <a class="pull-right">1,322</a>
              </li>
              <li class="list-group-item">
                <b>Following</b> <a class="pull-right">543</a>
              </li>
              <li class="list-group-item">
                <b>Friends</b> <a class="pull-right">13,287</a>
              </li>
            </ul>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin([
                    'id' => 'account-form',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>

                <!-- <div id="input" class="col-sm-offset-3 col-lg-9 text-center">
                    <?//= Html::img('@web/uploads/avatar/'.Yii::$app->User->identity->avatar, ['alt' => 'My logo']) ?>
                </div> -->

                
                <?= $form->field($model, 'username')->textInput(['disabled' => true]) ?>                                                                       
                                                                            
                <?= $form->field($model, 'name') ?>

                <?= $form->field($model, 'email') ?>                

                <?= $form->field($model, 'new_password')->passwordInput() ?>

                <?php
                // echo $form->field($model, 'avatar')->widget(FileInput::classname(), [
                //       'options' => ['accept' => 'image/*'],
                //       'pluginOptions'=>[
                //                         'allowedFileExtensions'=>['jpeg','jpg','gif','png'],
                //                         'showUpload' => false,
                //                         'showPreview' => true,
                //                         'showCaption' => true,
                //                         'showRemove' => true,
                //                        ],
                //     ]);   
                ?>

                <hr/>

                <?= $form->field($model, 'current_password')->passwordInput() ?>

                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-primary']) ?><br>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

        <?php if ($model->module->enableAccountDelete): ?>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('user', 'Delete account') ?></h3>
                </div>
                <div class="panel-body">
                    <p>
                        <?= Yii::t('user', 'Once you delete your account, there is no going back') ?>.
                        <?= Yii::t('user', 'It will be deleted forever') ?>.
                        <?= Yii::t('user', 'Please be certain') ?>.
                    </p>
                    <?= Html::a(Yii::t('user', 'Delete account'), ['delete'], [
                        'class' => 'btn btn-danger',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure? There is no going back'),
                    ]) ?>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>

