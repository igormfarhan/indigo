<?php

use yii\helpers\Html;
use yii\helpers\Url;


if (\Yii::$app->user->can('admin')) {

    $controller = 'admin';

} else if (\Yii::$app->user->can('user-treg')) {

    $controller = 'treg';

} else if (\Yii::$app->user->can('user-witel')) {

    $controller = 'witel';
 
} else if (\Yii::$app->user->can('user-ubis')) {

    $controller = 'ubis';

} else if (\Yii::$app->user->can('salesforce')) {

    $controller = 'salesforce';

}

?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= Html::img('@web/uploads/avatar/'.Yii::$app->User->identity->avatar, 
                    ['alt' => 'avatar','class'=>'img-circle']) 
                ?>
                <!-- <img src="<?= $directoryAsset ?>/img/user3-128x128.jpg" class="img-circle" alt="User Image"/> -->
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->User->identity->nama ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Application List', 'options' => ['class' => 'header']],
                    // ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/'.$controller.'/home']],
                    ['label' => 'Daftar Pelanggan', 'icon' => 'users', 'url' => ['/'.$controller.'/index']],
                    ['label' => 'Daftar Wilayah', 'icon' => 'map', 'url' => ['/'.$controller.'/wilayah']],
                    ['label' => 'Daftar SF', 'icon' => 'users','url' => ['/'.$controller.'/daftarsf'], 'visible' => Yii::$app->user->can('operator')],
                    ['label' => 'Profil', 'icon' => 'user', 'url' => ['/user/settings/account']],
                    ['label' => 'Homepage', 'icon' => 'home', 'url' => 'http://indi-go.id'],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
