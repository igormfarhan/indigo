<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\KotaKabupaten;
use common\models\Kecamatan;
use common\models\DesaKelurahan;

/* @var $this yii\web\View */
/* @var $model common\models\Pelanggan */
/* @var $form ActiveForm */

$kota = ArrayHelper::map(KotaKabupaten::find()->asArray()->all(), 'id', 'nama');
$kecamatan = ArrayHelper::map(Kecamatan::find()->asArray()->all(), 'id', 'nama');
$desa = ArrayHelper::map(DesaKelurahan::find()->asArray()->all(), 'id', 'nama');
?>

<div class="pelanggan-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

    <?= $form->field($model, 'nama_pelanggan')->label(false)->textInput(['placeholder' => 'Nama Lengkap',]) ?>
        <?= $form->field($model, 'no_handphone')->label(false)->textInput(['placeholder' => 'No. Handphone','maxlength' => true])  ?>
        <?= $form->field($model, 'kota_kabupaten_id')->label(false)->dropDownList($kota,
        [
          'prompt'=>'Kota/Kabupaten Lokasi Pemasangan',
          'id'=>'kota',
          'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl('pelanggan/listkec?id=').'"+$(this).val(), function( data ) {
            $( "select#kecamatan" ).html( data );
          });
          '
        ]);   ?>
        <?= $form->field($model, 'kecamatan_id')->label(false)->dropDownList($kecamatan,
        [
          'prompt'=>'Kecamatan Lokasi Pemasangan',
          'id'=>'kecamatan',
          'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl('site/listdesa?id=').'"+$(this).val(), function( data ) {
            $( "select#desa" ).html( data );
          });
          '
        ]);   ?>
        <?= $form->field($model, 'desa_kelurahan_id')->label(false)->dropDownList($desa,
        [
          'prompt'=>'Desa/Kelurahan Lokasi Pemasangan',
          'id'=>'desa',
        ]);   ?>

        <?= $form->field($model, 'address')->widget('kolyunya\yii2\widgets\MapInputWidget',[
            'key' => 'AIzaSyDHUEgb0jOwF49OEo4af5DUf8qpXHFmxwU',
            'latitude' => -6.8984729,
            'longitude' => 107.623498,
            'zoom' => 12,
            'height' => '500px',
            'animateMarker' => true,
            'enableSearchBar' => true,
            'alignMapCenter' => false,

          ])->label(false) ?>   
                 
        <!-- <?= $form->field($model, 'alamat')->label(false)->textArea(['placeholder' => 'Alamat Pemasangan',])  ?> -->

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
