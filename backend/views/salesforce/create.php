<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Pelanggan */

$this->title = 'Create Pelanggan';
$this->params['breadcrumbs'][] = ['label' => 'Pelanggans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggan-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
