<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PelangganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Calon Pelanggan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggan-index box box-primary">
    <div class="box-header with-border">
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'nama_pelanggan',
                [
                    'label' => 'Kecamatan',
                    'attribute' => 'kecamatan_id',
                    'value'=>'kecamatan.nama', //relation name with their attribute
                ],
                [
                    'label' => 'Desa/Kelurahan',
                    'attribute' => 'desa_kelurahan_id',
                    'value'=>'desaKelurahan.nama', //relation name with their attribute
                ],               
                'alamat',
                [
                    'attribute' => 'status_id',
                    'format' => 'raw',
                    'label' => 'Status Order',
                    'value'=> function ($model) {
                        return Html::tag('p', $model->orderStatus->status,['class' => $model->orderStatus->badge]);
                      }
                ],   
                [
                    'attribute' => 'hasil_id',
                    'format' => 'raw',
                    'label' => 'Status Hasil',
                    'value'=> function ($model) {
                        return Html::tag('p', $model->orderHasil->status,['class' => $model->orderHasil->badge]);
                      }
                ], 
                // 'salesforce_id',

                [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}', // {delete}',
                        'contentOptions' => function ($model, $key, $index, $column) {
                            return ['style' => 'min-width:50px'];
                        },
                        'buttons' => [
                            'view' => function ($url,$model,$key) {
                                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['class'=>'btn btn-warning']);
                            },
                            // 'update' => function ($url,$model) {
                            //     return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['class'=>'btn btn-warning']);
                            // },
                            'delete' => function ($url,$model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, 
                                    ['data-method' => 'post','class'=>'btn btn-danger','data-confirm'=> 'Are you sure?'],
                                    );
                                
                            },                    
                        ],
                    ],
            ],
        ]); ?>
    </div>
</div>
