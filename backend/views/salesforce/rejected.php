<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = '';
?>
<section class="content">
    
    <div class="text-center">
    
    <?= Html::img('@web/img/rejected.png', ['alt'=>'Jawara','class'=>'img-responsive', 'style'=>'width: 500px;margin:0 auto']);?>
    <?= Html::a('Update Data Profil', ['/user/settings/account'], 
                ['class' => 'btn btn-block btn-lg btn-primary','style'=>'margin-top:20px']) ?>
    
    </div>

</section>
