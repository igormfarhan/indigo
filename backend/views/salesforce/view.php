<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\KotaKabupaten;
use common\models\Kecamatan;
use common\models\DesaKelurahan;
use common\models\OrderStatus;

$status1 = ArrayHelper::map(OrderStatus::find()->asArray()->where(['<','id',200])->all(), 'id', 'status');
$status2 = ArrayHelper::map(OrderStatus::find()->asArray()->where(['>','id',200])->all(), 'id', 'status');
$hasil = [];
/* @var $this yii\web\View */
/* @var $model common\models\Pelanggan */

$this->title = $model->nama_pelanggan;
$this->params['breadcrumbs'][] = ['label' => 'Pelanggans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggan-view box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'nama_pelanggan',
                'no_handphone',
                'kotaKabupaten.nama',
                'kecamatan.nama',
                'desaKelurahan.nama',
                'alamat',
                [
                    'attribute' => 'koordinat',
                    'format' => 'raw',
                    'label' => 'Koordinat',
                    'value' => function ($model) {
                      return Html::a($model->koordinat, 'https://www.google.com/maps/@'.$model->koordinat.',15z');
                    }
                  ],
                'tanggal_submit:datetime',
                [
                    'attribute' => 'status_id',
                    'format' => 'raw',
                    'label' => 'Status Order',
                    'value' => $model->orderStatus->status,
                ],
                [
                    'attribute' => 'hasil_id',
                    'format' => 'raw',
                    'label' => 'Hasil Order',
                    'value' => $model->orderHasil->status,
                ],                
                
                'nomor_myir_sc',
            ],
        ]) ?>
    </div>
</div>

<div class="status-update">
<div class="pelanggan-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'status_id')->dropDownList($status1,
        [
          'prompt'=>'Silahkan pilih status',
          'id'=>'status_order',
          
        ]);   ?>

        <?= $form->field($model, 'hasil_id')->dropDownList($status2,
        [
          'prompt'=>'Silahkan ubah status order terlebih dahulu',
        ]);   ?>

        <?= $form->field($model, 'nomor_myir_sc')->textInput(['placeholder' => 'Masukkan Nomor MYIR atau SC',]);  ?>
                 

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

</div>

<?php
$script = <<< JS

$(document).ready(function () {
    $(document.body).getElementById('#hasil_order').hide();
    $(document.body).on('change', '#status_order', function () {
        var val = $('#status_order').val();
        if(val == 101 ) {
          $('#hasil_order').hide();
        } else {
          $('#hasil_order').show();
        }
    });
});

JS;
$this->registerJs($script);
?>

