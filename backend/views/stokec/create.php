<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UbisKecamatan */

$this->title = 'Create Ubis Kecamatan';
$this->params['breadcrumbs'][] = ['label' => 'Ubis Kecamatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ubis-kecamatan-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
