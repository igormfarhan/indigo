<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Witel;
use common\models\Ubis;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UbisKecamatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Index UBIS-Kecamatan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ubis-kecamatan-index box box-primary">
    <div class="box-header with-border">
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'label' => 'Nama Witel',
                    'attribute' => 'witel',
                    'filter' => ArrayHelper::map(Witel::find()->asArray()->all(), 'id', 'nama'),
                    'value'=> 'witel.nama', //relation name with their attribute
                ], 
                [
                    'label' => 'Nama UBIS',
                    'attribute' => 'ubis',
                    'filter' => ArrayHelper::map(Ubis::listUbis($searchModel->witel), 'id', 'nama'),
                    'value'=> 'ubis.nama', //relation name with their attribute
                ], 
                [
                    'label' => 'Kode UBIS',
                    'attribute' => 'ubis_id',
                    'value'=> 'ubis.kode_ubis', //relation name with their attribute
                ], 
                [
                    'label' => 'Kecamatan',
                    'attribute' => 'kecamatan_id',
                    'value'=> 'kecamatan.nama', //relation name with their attribute
                ], 
                
                
                

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
