<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UbisKecamatan */

$this->title = 'Update Ubis Kecamatan: ' . $model->ubis_id;
$this->params['breadcrumbs'][] = ['label' => 'Ubis Kecamatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ubis_id, 'url' => ['view', 'ubis_id' => $model->ubis_id, 'kecamatan_id' => $model->kecamatan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ubis-kecamatan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
