<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UbisKecamatan */

$this->title = $model->ubis_id;
$this->params['breadcrumbs'][] = ['label' => 'Ubis Kecamatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ubis-kecamatan-view box box-primary">
    <div class="box-header">
        <?= Html::a('Update', ['update', 'ubis_id' => $model->ubis_id, 'kecamatan_id' => $model->kecamatan_id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete', ['delete', 'ubis_id' => $model->ubis_id, 'kecamatan_id' => $model->kecamatan_id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ubis_id',
                'kecamatan_id',
            ],
        ]) ?>
    </div>
</div>
