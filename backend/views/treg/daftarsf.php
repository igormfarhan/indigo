<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Witel;
use common\models\Ubis;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Button;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Sales Force';
$this->params['breadcrumbs'][] = $this->title;


if(Ubis::listUbis($searchModel->witel)){
    $ubislist = Ubis::listUbis($searchModel->witel);
} else {
    $ubislist = Ubis::find()->asArray()->all();
}

$check = ArrayHelper::map(Ubis::listUbis(Yii::$app->user->identity->witel_id), 'id', 'nama');
$array = [0 => 'NOT SET'];
$ubis = $check+$array;


?>
<div class="user-index box box-primary">
    <div class="box-header with-border">
    <div class="pull-left">
    </div>
    <!-- <div class="pull-right">
        <?= Html::a('Tambah SF', ['tambahsf'], ['class' => 'btn btn-primary']) ?>
    </div> -->
    
    
    </div>



    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Nama Witel',
                    'attribute' => 'witel',
                    'filter' => ArrayHelper::map(Witel::find()->asArray()->all(), 'id', 'nama'),
                    'value'=> 'witel.nama', //relation name with their attribute
                ],
                [
                    'label' => 'Nama UBIS',
                    'attribute' => 'ubis',
                    'filter' => ArrayHelper::map($ubislist, 'id', 'nama'),
                    'value'=> 'ubis.nama', //relation name with their attribute
                ],
                'nik',
                'nama',
				'no_handphone',
                'email:email',
                // 'order',                 
                // 'password_hash',
                // 'auth_key',
                // 'confirmed_at',
                // 'unconfirmed_email:email',
                // 'blocked_at',
                // 'registration_ip',
                // 'created_at',
                // 'updated_at',
                // 'last_login_at',
                // 'avatar',
                [
                    'label' => 'Lenght of Stay',
                    'attribute' => 'active_at',
                    'format'=> 'raw',
                    'filter' => array('0' =>'>6 BULAN','1'=>'>1 TAHUN'),
                    'value' => function ($model) {
                        return Yii::$app->formatter->format($model->active_at, 'relativeTime');
                    }
                ],
                [
                    'label' => 'Status',
                    'attribute' => 'status',
                    'format' => 'raw',
                    'filter' => array('0' =>'INACTIVE','1'=>'ACTIVE','99'=>'NEED APPROVAL','98'=>'REFUSED'),
                    'value'=> function ($model) {
                        if($model->status == 1) {
                            return Html::tag('p', $model->statusText,[
                                'class' => 'btn btn-xs btn-success btn-block',
                                ]);
                        } elseif ($model->status == 0) {
                            return Html::tag('p', $model->statusText,[
                                'class' => 'btn btn-xs btn-danger btn-block',
                                ]);
                        } elseif ($model->status == 99) {
                            return Html::tag('p', $model->statusText,[
                                'class' => 'btn btn-xs btn-primary btn-block',
                                ]);
                        }
                        elseif ($model->status == 98) {
                            return Html::tag('p', $model->statusText,[
                                'class' => 'btn btn-xs btn-warning btn-block',
                                ]);
                        }
                      }, 
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{updatesf}',
                    'buttons' => [
                        'updatesf' => function ($url,$model,$key) {
                                return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['class'=>'btn btn-xs btn-warning']);
                        },
                    ],
                ],

            ],
        ]); ?>
    </div>
</div>
