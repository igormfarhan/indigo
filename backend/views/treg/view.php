<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\KotaKabupaten;
use common\models\Kecamatan;
use common\models\DesaKelurahan;
use common\models\OrderStatus;

$status = ArrayHelper::map(OrderStatus::find()->asArray()->where(['<','id',200])->all(), 'id', 'status');
$hasil = ArrayHelper::map(OrderStatus::find()->asArray()->where(['>','id',200])
                                                        ->andWhere(['<','id',300])
                                                        ->all(), 'id', 'status');


/* @var $this yii\web\View */
/* @var $model common\models\Pelanggan */

$this->title = $model->nama_pelanggan;
$this->params['breadcrumbs'][] = ['label' => 'Pelanggans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggan-view box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'salesforce.nama',
                    'format' => 'raw',
                    'label' => 'Salesforce',
                ],
                'nama_pelanggan',
                'no_handphone',
                'kotaKabupaten.nama',
                'kecamatan.nama',
                'desaKelurahan.nama',
                'alamat',
                [
                    'attribute' => 'koordinat',
                    'format' => 'raw',
                    'label' => 'Koordinat',
                    'value' => function ($model) {
                      return Html::a($model->koordinat, 'https://www.google.com/maps/search/?api=1&query='.$model->koordinat);
                      // https://www.google.com/maps/search/?api=1&query=47.5951518,-122.3316393
                    }
                ],
                'tanggal_submit:datetime',
                [
                    'attribute' => 'status_id',
                    'format' => 'raw',
                    'label' => 'Status Order',
                    'value' => $model->orderStatus->status,
                ],
                [
                    'attribute' => 'hasil_id',
                    'format' => 'raw',
                    'label' => 'Hasil Order',
                    'value' => $model->orderHasil->status,
                ],                
                
                'nomor_myir_sc',
            ],
        ]) ?>
    </div>
</div>

