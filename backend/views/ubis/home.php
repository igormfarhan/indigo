<?php

/* @var $this yii\web\View */

use common\models\Pelanggan;
use common\models\User;
use yii\helpers\Html;

$this->title = 'Dashboard';

$subQuery = User::find()->select('id')->where(['ubis_id'=> Yii::$app->user->identity->ubis_id])
->andWhere(['>','id',200]);

?>
      <!-- Small boxes (Stat box) -->
<div class="row">
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner text-center">
        <h3>
        <?php
          echo Pelanggan::find()->where(['salesforce_id'=>$subQuery])
                                ->count();              
        ?>
        </h3>
        <h4>Jumlah Hit</h4>
      </div>
      <div class="icon">
        <i class="fa fa-chevron-circle-up"></i>
      </div>
      <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>', ['dashboard','id'=>1], 
                                    ['class'=>'small-box-footer'],
                                    ); ?>    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner text-center">
        <h3>
        <?php
          echo Pelanggan::find()->where(['salesforce_id'=>$subQuery,
                                         'status_id'=>101])->count();              
        ?>              
        </h3>
        <h4>Belum Lapor Follow Up</h4>
      </div>
      <div class="icon">
        <i class="fa fa-question-circle"></i>
      </div>
      <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>', ['dashboard','id'=>2], 
                                    ['class'=>'small-box-footer'],
                                    ); ?>    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-orange">
      <div class="inner text-center">
        <h3>
        <?php
          echo Pelanggan::find()->where(['salesforce_id'=>$subQuery,
                                         'hasil_id'=>201])->count();
                           
        ?>    
        </h3>
        <h4>Belum Lapor Hasil</h4>
      </div>
      <div class="icon">
        <i class="fa fa-question-circle"></i>
      </div>
      <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>', ['dashboard','id'=>3], 
                                    ['class'=>'small-box-footer'],
                                    ); ?>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner text-center">
        <h3>
        <?php
          echo Pelanggan::find()->where(['salesforce_id'=>$subQuery,
                                         'hasil_id'=>203])->count();                
        ?>    
        </h3>
        <h4>Belum Deal</h4>
      </div>
      <div class="icon">
        <i class="fa fa-times-circle"></i>
      </div>
      <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>', ['dashboard','id'=>4], 
                                    ['class'=>'small-box-footer'],
                                    ); ?>    </div>
  </div>
  <!-- ./col -->
</div>

      
