<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Witel;
use common\models\Ubis;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Sales Force';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <div class="box-header with-border">
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Nama Witel',
                    'attribute' => 'witel',
                    'filter' => ArrayHelper::map(Witel::find()->asArray()->all(), 'id', 'nama'),
                    'value'=> 'witel.nama', //relation name with their attribute
                ],
                [
                    'label' => 'Nama UBIS',
                    'attribute' => 'ubis',
                    'filter' => ArrayHelper::map(Ubis::listUbis($searchModel->witel), 'id', 'nama'),
                    'value'=> 'ubis.nama', //relation name with their attribute
                ], 
                [
                    'label' => 'Kode UBIS',
                    'attribute' => 'ubis_id',
                    'value'=> 'ubis.kode_ubis', //relation name with their attribute
                ],
                'nik',
                'nama',
                'email:email',
                // 'order',                 
                // 'password_hash',
                // 'auth_key',
                // 'confirmed_at',
                // 'unconfirmed_email:email',
                // 'blocked_at',
                // 'registration_ip',
                // 'created_at',
                // 'updated_at',
                // 'last_login_at',
                // 'avatar',
                [
                    'label' => 'Status',
                    'attribute' => 'status',
                    'filter' =>array('0' =>'INACTIVE','1'=>'ACTIVE'),
                    'value'=> 'status', 
                ],
            ],
        ]); ?>
    </div>
</div>
