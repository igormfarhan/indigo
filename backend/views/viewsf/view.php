<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view box box-primary">
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'nik',
                'nama',
                'no_handphone',
                'email:email',
                'order',
                'ubis_id',
                'password_hash',
                'auth_key',
                'confirmed_at',
                'unconfirmed_email:email',
                'blocked_at',
                'registration_ip',
                'created_at:datetime',
                'updated_at:datetime',
                'last_login_at',
                'avatar',
                'status',
            ],
        ]) ?>
    </div>
</div>
