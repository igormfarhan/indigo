<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\User $user
 */
use common\models\Ubis;
use yii\helpers\ArrayHelper;

$ubis = ArrayHelper::map(Ubis::find()->asArray()->all(), 'id', 'nama');

?>

<?= $form->field($user, 'nik')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'nama')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'no_handphone')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'ubis_id')->dropDownList($ubis) ?>
