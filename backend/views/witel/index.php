<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Ubis;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $searchModel common\models\PelangganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Calon Pelanggan';

$witel_id = Yii::$app->user->identity->ubis_id;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],

   
    [
        'label' => 'Nama UBIS',
        'attribute' => 'ubis',
        'filter' => ArrayHelper::map(Ubis::listUbis($witel_id), 'id', 'nama'),
        'value'=> 'ubis.nama', //relation name with their attribute
    ], 
    [
        'label' => 'Salesforce',
        'attribute' => 'salesforce_id',
        'value'=> 'salesforce.nama', //relation name with their attribute
    ],  
    'nama_pelanggan',
    [
        'label' => 'Kecamatan',
        'attribute' => 'kecamatan_id',
        'value'=>'kecamatan.nama', //relation name with their attribute
    ],
    [
        'label' => 'Desa/Kelurahan',
        'attribute' => 'desa_kelurahan_id',
        'value'=>'desaKelurahan.nama', //relation name with their attribute
    ],               
    'alamat',
    'tanggal_submit:date',
    [
        'attribute' => 'status_order',
        'format' => 'raw',
        'label' => 'Status Order',
        'filter' => array('101' =>'Belum follow up','102'=>'Sudah follow up'),
        'value'=> function ($model) {
            return Html::tag('p', $model->orderStatus->status,['class' => $model->orderStatus->badge]);
          }
    ],   
    [
        'attribute' => 'status_hasil',
        'format' => 'raw',
        'label' => 'Status Hasil',
        'filter' => array('201' =>'Belum dilaporkan','202'=>'Deal pasang','203'=>'Belum deal pasang','204'=>'Tidak ada jaringan'),
        'value'=> function ($model) {
            return Html::tag('p', $model->orderHasil->status,['class' => $model->orderHasil->badge]);
          }
    ], 
    // 'salesforce_id',

    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view} {delete}',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'min-width:50px'];
        },
        'buttons' => [
            'view' => function ($url,$model,$key) {
                    return Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['class'=>'btn btn-primary']);
            },
            // 'update' => function ($url,$model) {
            //     return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['class'=>'btn btn-warning']);
            // },
            'delete' => function ($url,$model) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, 
                    ['data-method' => 'post','class'=>'btn btn-danger','data-confirm'=> 'Apakah kamu yakin?'],
                    );
                
            },                    
        ],
    ],
    ];
?>

<div class="pelanggan-index box box-primary">
<div class="box-header with-border">
    <div class="row">
    <div class="pull-left col-lg-6">
        <?php echo ExportMenu::widget(['dataProvider' => $dataProvider,'columns' => $gridColumns,]); ?>
    </div>
    <div class="pull-right col-lg-6">
    <div class="row">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal','class' => 'form-horizontal' ,]); ?>

    <div class="col-lg-8">
    <?php
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'start_date',
            'attribute2' => 'end_date',
            'options' => ['placeholder' => 'Periode Awal'],
            'options2' => ['placeholder' => 'Periode Akhir'],
            'type' => DatePicker::TYPE_RANGE,
            'form' => $form,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
            ]
        ]);
        
    ?>
    
    </div>

    <div class="col-lg-4 ">        
    <?php
    echo Html::submitButton('Cari', ['class' => 'btn btn-success btn-block']);    
    ActiveForm::end(); ?>
    </div>  
  
    </div> <!-- Row pull right    -->
    </div> <!-- Pull right   -->
    </div> <!-- Row pull left and pull right   -->
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => $gridColumns,
        ]); ?>
    </div>
</div>
