<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 */

use common\models\Ubis;
use yii\helpers\ArrayHelper;

$ubis = ArrayHelper::map(Ubis::find()->asArray()->all(), 'id', 'nama');

$this->title = Yii::t('user', 'Tambah Salesforce');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['witel/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pelanggan-index box box-primary">
    <div class="box-header with-border">

<?php 
    $module = Yii::$app->getModule('user');
    if ($module->enableFlashMessages): ?>
    <div class="row">
        <div class="col-xs-12">
            <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message): ?>
                <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])): ?>
                    <?= Alert::widget([
                        'options' => ['class' => 'alert-dismissible alert-' . $type],
                        'body' => $message
                    ]) ?>
                <?php endif ?>
            <?php endforeach ?>
        </div>
    </div>
<?php endif ?>



<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= Nav::widget([
                    'options' => [
                        'class' => 'nav-pills nav-stacked',
                    ],
                    'items' => [
                        ['label' => 'Daftar Salesforce', 'url' => ['/witel/sflist']],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="alert alert-info">
                    <?= Yii::t('user', 'Credentials will be sent to the user by email') ?>.
                    <?= Yii::t('user', 'A password will be generated automatically if not provided') ?>.
                </div>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-sm-9',
                        ],
                    ],
                ]); ?>
                <?= $form->field($user, 'nik')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($user, 'no_handphone')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($user, 'ubis_id')->dropDownList($ubis, [
                    'prompt'=>'PILIH UBIS',
                 ]) ?>
                <?= $form->field($user, 'password')->passwordInput() ?>
               

                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
</div>
</div>


