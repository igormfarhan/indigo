<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Witel;
use common\models\Ubis;
use backend\models\User;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Button;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UbisKecamatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Wilayah';
$this->params['breadcrumbs'][] = $this->title;

$ubis_id = Yii::$app->user->identity->ubis_id;
$witel = Witel::find()->where(['id'=>$ubis_id])->one();

?>
<div class="ubis-kecamatan-index box box-primary">
    <div class="box-header with-border">
    <?=  Button::widget([
        'label' => 'WITEL : '. $witel->nama,
        'options' => ['class' => 'btn btn-success']]);?>    
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Kota/Kabupaten',
                    'attribute' => 'id',
                    'value'=> 'kotaKabupaten.nama', //relation name with their attribute
                ], 
                [
                    'label' => 'Kecamatan',
                    'attribute' => 'id',
                    'value'=> 'kecamatan.nama', //relation name with their attribute
                ], 
                [
                    'label' => 'Desa/Kelurahan',
                    'attribute' => 'id',
                    'value'=> 'nama', //relation name with their attribute
                ], 
            ],
        ]); ?>
    </div>
</div>
