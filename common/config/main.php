<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@files' => dirname(__DIR__) . '/../',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'timeZone' => 'Asia/Jakarta',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=indigo',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mailtrap.io',
                'username' => '0c65419a10209d',
                'password' => 'ce533b875b603e',
                'port' => '2525',
                'encryption' => 'tls',
            ],
        ],

        'mailers' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'indi-go.id',
                'username' => 'notification@indi-go.id',
                'password' => 'Emailjapati',
                'port' => '25',
                'encryption' => 'tls',
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                // ...
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',            
        ], 
    ],
];
