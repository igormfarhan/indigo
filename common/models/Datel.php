<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "datel".
 *
 * @property string $id
 * @property string|null $witel_id
 * @property string|null $nama
 *
 * @property Witel $witel
 * @property Ubis[] $ubiss
 */
class Datel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'datel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'string', 'max' => 4],
            [['witel_id'], 'string', 'max' => 2],
            [['nama'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['witel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Witel::className(), 'targetAttribute' => ['witel_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'witel_id' => 'Witel ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * Gets query for [[Witel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWitel()
    {
        return $this->hasOne(Witel::className(), ['id' => 'witel_id']);
    }

    /**
     * Gets query for [[Ubiss]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUbiss()
    {
        return $this->hasMany(Ubis::className(), ['datel_id' => 'id']);
    }
}
