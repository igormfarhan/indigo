<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "desa_kelurahan".
 *
 * @property string $id
 * @property string $kecamatan_id
 * @property string $nama
 *
 * @property Kecamatan $kecamatan
 * @property Pelanggan[] $pelanggans
 * @property SalesforceAssignment[] $salesforceAssignments
 */
class DesaKelurahan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'desa_kelurahan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kecamatan_id', 'nama'], 'required'],
            [['id'], 'string', 'max' => 10],
            [['kecamatan_id'], 'string', 'max' => 7],
            [['nama'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['kecamatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['kecamatan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kecamatan_id' => 'Kecamatan ID',
            'nama' => 'Desa/Kelurahan',
        ];
    }

    public function getKotaKabupaten()
    {
        return $this->hasOne(KotaKabupaten::classname(),['id'=>'kota_kabupaten_id'])
                    ->via('kecamatan');
    }
    /**
     * Gets query for [[Kecamatan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatan()
    {
        return $this->hasOne(Kecamatan::className(), ['id' => 'kecamatan_id']);
    }

    /**
     * Gets query for [[Pelanggans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggans()
    {
        return $this->hasMany(Pelanggan::className(), ['desa_kelurahan_id' => 'id']);
    }

    /**
     * Gets query for [[SalesforceAssignments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSalesforceAssignments()
    {
        return $this->hasMany(SalesforceAssignment::className(), ['desa_kelurahan_id' => 'id']);
    }
}
