<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kecamatan".
 *
 * @property string $id
 * @property string $kota_kabupaten_id
 * @property string $name
 *
 * @property DesaKelurahan[] $desaKelurahans
 * @property KotaKabupaten $kotaKabupaten
 * @property Pelanggan[] $pelanggans
 */
class Kecamatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kecamatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kota_kabupaten_id', 'nama'], 'required'],
            [['id'], 'string', 'max' => 7],
            [['kota_kabupaten_id'], 'string', 'max' => 4],
            [['nama'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['kota_kabupaten_id'], 'exist', 'skipOnError' => true, 'targetClass' => KotaKabupaten::className(), 'targetAttribute' => ['kota_kabupaten_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kota_kabupaten_id' => 'Kota Kabupaten ID',
            'nama' => 'Kecamatan',
        ];
    }

    /**
     * Gets query for [[DesaKelurahans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDesaKelurahans()
    {
        return $this->hasMany(DesaKelurahan::className(), ['kecamatan_id' => 'id']);
    }

    /**
     * Gets query for [[KotaKabupaten]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKotaKabupaten()
    {
        return $this->hasOne(KotaKabupaten::className(), ['id' => 'kota_kabupaten_id']);
    }

    /**
     * Gets query for [[Pelanggans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggans()
    {
        return $this->hasMany(Pelanggan::className(), ['kecamatan_id' => 'id']);
    }
}
