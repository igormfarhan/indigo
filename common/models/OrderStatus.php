<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_status".
 *
 * @property int $id
 * @property string|null $status
 * @property string|null $badge
 *
 * @property Pelanggan[] $pelanggans
 * @property Pelanggan[] $pelanggans0
 */
class OrderStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'badge'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'badge' => 'Badge',
        ];
    }

    /**
     * Gets query for [[Pelanggans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggans()
    {
        return $this->hasMany(Pelanggan::className(), ['hasil_id' => 'id']);
    }

    /**
     * Gets query for [[Pelanggans0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggans0()
    {
        return $this->hasMany(Pelanggan::className(), ['status_id' => 'id']);
    }
}
