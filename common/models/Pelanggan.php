<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pelanggan".
 *
 * @property int $id
 * @property string $nama_pelanggan
 * @property string $no_handphone
 * @property string $kota_kabupaten_id
 * @property string $kecamatan_id
 * @property string $desa_kelurahan_id
 * @property string $alamat
 * @property int $salesforce_id
 *
 * @property DesaKelurahan $desaKelurahan
 * @property Kecamatan $kecamatan
 * @property KotaKabupaten $kotaKabupaten
 * @property User $salesforce
 */
class Pelanggan extends \yii\db\ActiveRecord
{
    public $address;
    // public $longitude = -6.8984729;
    // public $latitude = 107.623498;
    public $longitude;
    public $latitude;
    public $start_date;
    public $end_date;
    public $Captcha;
    public $reCaptcha;
    public $reCaptcha2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pelanggan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salesforce_id'], 'required'],
            [['nama_pelanggan'], 'required', 'message' => 'Nama harus diisi'],
            [['no_handphone'], 'required', 'message' => 'No. HP harus diisi'],
            [['kota_kabupaten_id'], 'required', 'message' => 'Alamat Kota/Kabupaten harus diisi'],
            [['kecamatan_id'], 'required', 'message' => 'Alamat Kecamatan harus diisi'],
            [['desa_kelurahan_id'], 'required', 'message' => 'Alamat Desa/Kelurahan harus diisi'],
            [['alamat'], 'required', 'message' => 'Detail alamat harus diisi'],
            // [['Captcha'], 'captcha'],
            [['id', 'salesforce_id','status_id','hasil_id'], 'integer'],
            [['nama_pelanggan', 'alamat','koordinat','nomor_myir_sc'], 'string', 'max' => 255],
            [['no_handphone'], 'string','min'=> 10,'max' => 14],
            [['kota_kabupaten_id'], 'string', 'max' => 4],
            [['kecamatan_id'], 'string', 'max' => 7],
            [['desa_kelurahan_id'], 'string', 'max' => 10],
            [['id'], 'unique'],
            [['desa_kelurahan_id'], 'exist', 'skipOnError' => true, 'targetClass' => DesaKelurahan::className(), 'targetAttribute' => ['desa_kelurahan_id' => 'id']],
            [['kecamatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['kecamatan_id' => 'id']],
            [['kota_kabupaten_id'], 'exist', 'skipOnError' => true, 'targetClass' => KotaKabupaten::className(), 'targetAttribute' => ['kota_kabupaten_id' => 'id']],
            [['salesforce_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['salesforce_id' => 'id']],
            [['start_date','end_date'],'safe'],
            ['nomor_myir_sc', 'required', 'when' => function ($model) {
                return $model->hasil_id == 202? true:false;
            },'whenClient'=>"function(){
                if($('#hasil_id').val() == 202)
                {true} else {false}
            }"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pelanggan' => 'Nama Pelanggan',
            'no_handphone' => 'No Handphone',
            'kota_kabupaten_id' => 'Kota Kabupaten ID',
            'kecamatan_id' => 'Kecamatan ID',
            'desa_kelurahan_id' => 'Desa Kelurahan ID',
            'alamat' => 'Alamat',
            'salesforce_id' => 'Salesforce ID',
            'status_id' => 'Status Order',
            'hasil_id' => 'Hasil Order',
            'nomor_myir_sc' => 'Nomor MYIR / SC',
            'start_date' => 'Periode Awal',
            'end_date' => 'Periode Akhir',
        ];
    }

    /**
     * Gets query for [[DesaKelurahan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDesaKelurahan()
    {
        return $this->hasOne(DesaKelurahan::className(), ['id' => 'desa_kelurahan_id']);
    }

    /**
     * Gets query for [[Kecamatan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatan()
    {
        return $this->hasOne(Kecamatan::className(), ['id' => 'kecamatan_id']);
    }

    /**
     * Gets query for [[KotaKabupaten]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKotaKabupaten()
    {
        return $this->hasOne(KotaKabupaten::className(), ['id' => 'kota_kabupaten_id']);
    }

    /**
     * Gets query for [[Salesforce]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSalesforce()
    {
        return $this->hasOne(User::className(), ['id' => 'salesforce_id']);
    }

    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    public function getOrderHasil()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'hasil_id']);
    }

    public function getUbisKecamatan()
    {
        return $this->hasOne(UbisKecamatan::className(), ['id' => 'ubis_id']);
    }

    public function getUbis()
    {
        return $this->hasOne(Ubis::className(), ['id' => 'ubis_id'])
                    ->via('salesforce');
    }

    public function getDatel()
    {
        return $this->hasOne(Ubis::className(), ['id' => 'datel_id'])
                    ->via('ubis');
    }

    public function getWitel()
    {
        return $this->hasOne(Ubis::className(), ['id' => 'witel_id'])
                    ->via('datel');
    }

    
}
