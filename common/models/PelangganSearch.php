<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pelanggan;

/**
 * PelangganSearch represents the model behind the search form of `common\models\Pelanggan`.
 */
class PelangganSearch extends Pelanggan
{
    /**
     * @inheritdoc
     */

    public $salesforce;
    public $kota_kabupaten;
    public $kecamatan;
    public $desa_kelurahan;
    public $ubis;
    public $witel;
    public $status_order;
    public $status_hasil;
    


    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['salesforce_id','nama_pelanggan', 'no_handphone', 'kota_kabupaten_id', 'kecamatan_id', 'desa_kelurahan_id', 'alamat'], 'safe'],
            [['salesforce','kota_kecamatan','kecamatan','desa_kelurahan'],'safe'],
            [['ubis','witel','status_order','status_hasil'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$query)
    {
        // $query = Pelanggan::find();

        // add conditions that should always apply here
        $query->joinWith(['kotaKabupaten','kecamatan','salesforce','ubis.datel.witel']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['ubis'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['ubis.nama' => SORT_ASC],
            'desc' => ['ubis.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['witel'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['witel.nama' => SORT_ASC],
            'desc' => ['witel.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['salesforce'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['salesforce.nama' => SORT_ASC],
            'desc' => ['salesforce.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['kota_kabupaten'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['kotaKabupaten.name' => SORT_ASC],
            'desc' => ['kotaKabupaten.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['kecamatan'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['kecamatan.nama' => SORT_ASC],
            'desc' => ['kecamatan.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['desaKelurahan'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['desaKelurahan.nama' => SORT_ASC],
            'desc' => ['desaKelurahan.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['status_order'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['id' => SORT_ASC],
            'desc' => ['id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['status_hasil'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['id' => SORT_ASC],
            'desc' => ['id' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ubis.id' => $this->ubis,
            'witel.id' => $this->witel,
            'kota_kabupaten.id' => $this->kota_kabupaten_id,
            'kecamatan.id' => $this->kecamatan_id,
            'desa_kelurahan.id' => $this->desa_kelurahan_id,
            // 'salesforce_id' => $this->salesforce_id,
            'status_id' => $this->status_order,
            'hasil_id' => $this->status_hasil,
        ]);

        $query
            ->andFilterWhere(['like', 'nama_pelanggan', $this->nama_pelanggan]) 
            ->andFilterWhere(['like', 'no_handphone', $this->no_handphone])
            // ->andFilterWhere(['like', 'kota_kabupaten_id', $this->kota_kabupaten_id])
            // ->andFilterWhere(['like', 'kecamatan_id', $this->kecamatan_id])
            // ->andFilterWhere(['like', 'desa_kelurahan_id', $this->desa_kelurahan_id])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'user.nama', $this->salesforce_id])
            ->andFilterWhere(['like', 'kecamatan.nama', $this->kecamatan_id]);
            // ->andFilterWhere(['like', 'desa_kelurahan.nama', $this->desa_kelurahan_id]);

        return $dataProvider;
    }

    
}
