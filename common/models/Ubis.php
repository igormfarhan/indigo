<?php

namespace common\models;
use backend\models\User;

use Yii;

/**
 * This is the model class for table "ubis".
 *
 * @property string $id
 * @property string|null $datel_id
 * @property string|null $kode_ubis
 * @property string|null $nama
 *
 * @property Datel $datel
 * @property ubisKecamatan[] $ubisKecamatans
 * @property Kecamatan[] $kecamatans
 */
class Ubis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'string', 'max' => 6],
            [['datel_id'], 'string', 'max' => 4],
            [['kode_ubis'], 'string', 'max' => 128],
            [['nama'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['datel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Datel::className(), 'targetAttribute' => ['datel_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datel_id' => 'Datel ID',
            'kode_ubis' => 'Kode Ubis',
            'nama' => 'Nama',
        ];
    }

    /**
     * Gets query for [[Datel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWitel()
    {
        return $this->hasOne(Datel::className(), ['id' => 'witel_id'])
        ->via('datel');
    }
    
     public function getDatel()
    {
        return $this->hasOne(Datel::className(), ['id' => 'datel_id']);
    }

    /**
     * Gets query for [[UbisKecamatans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUbisKecamatans()
    {
        return $this->hasMany(UbisKecamatan::className(), ['ubis_id' => 'id']);
    }

    /**
     * Gets query for [[Kecamatans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatans()
    {
        return $this->hasMany(Kecamatan::className(), ['id' => 'kecamatan_id'])->viaTable('ubis_kecamatan', ['ubis_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasMany(User::className(), ['ubis_id' => 'id']);
    }

    public function listUbis($witel)
    {
        $ubis = Ubis::find()->asArray()->joinWith('datel')->where(['witel_id' => $witel])
                                                        ->orderBy('id ASC')
                                                        ->all();
                                       
        return $ubis;
    }
}
