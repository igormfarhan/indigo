<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ubis_kecamatan".
 *
 * @property int $ubis_id
 * @property string $kecamatan_id
 *
 * @property Kecamatan $kecamatan
 * @property Ubis $ubis
 */
class UbisKecamatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubis_kecamatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ubis_id', 'kecamatan_id'], 'required'],
            [['ubis_id'], 'integer'],
            [['kecamatan_id'], 'string', 'max' => 7],
            [['ubis_id', 'kecamatan_id'], 'unique', 'targetAttribute' => ['ubis_id', 'kecamatan_id']],
            [['kecamatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['kecamatan_id' => 'id']],
            [['ubis_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ubis::className(), 'targetAttribute' => ['ubis_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ubis_id' => 'Ubis ID',
            'kecamatan_id' => 'Kecamatan ID',
        ];
    }

    /**
     * Gets query for [[Kecamatan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatan()
    {
        return $this->hasOne(Kecamatan::className(), ['id' => 'kecamatan_id']);
    }

    /**
     * Gets query for [[Ubis]].
     *
     * @return \yii\db\ActiveQuery
     */

    public function getSales()
    {
        return $this->hasOne(User::className(), ['ubis_id' => 'ubis_id']);
    }

    public function getPelanggan()
    {
        return $this->hasMany(Pelanggan::className(), ['ubis_id' => 'id']);
    }

    public function getWitel()
    {
        return $this->hasOne(Witel::classname(),['id'=>'witel_id'])
                    ->via('datel');
    }

    public function getDatel()
    {
        return $this->hasOne(Datel::classname(),['id'=>'datel_id'])
                    ->via('ubis');
    }

    public function getUbis()
    {
        return $this->hasOne(Ubis::className(), ['id' => 'ubis_id']);
    }

}
