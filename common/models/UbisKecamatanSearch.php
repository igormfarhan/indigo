<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UbisKecamatan;

/**
 * UbisKecamatanSearch represents the model behind the search form of `common\models\UbisKecamatan`.
 */
class UbisKecamatanSearch extends UbisKecamatan
{
    public $witel;
    public $ubis;
    public $ubis_kode;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ubis_id', 'kecamatan_id'], 'safe'],
            [['witel','ubis','ubis_kode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$query)
    {
        // $query = UbisKecamatan::find();

        // add conditions that should always apply here
        $query->joinWith('ubis.datel.witel');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        
            ]);
        
        $dataProvider->sort->attributes['witel'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['witel.nama' => SORT_ASC],
            'desc' => ['witel.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ubis'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['ubis.nama' => SORT_ASC],
            'desc' => ['ubis.nama' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([

            'witel_id' => $this->witel,
            'ubis_id' => $this->ubis

        ]);

        // grid filtering conditions
        $query->andFilterWhere(['like', 'ubis_id', $this->ubis_id])
            ->andFilterWhere(['like', 'kecamatan_id', $this->kecamatan_id]);

        return $dataProvider;
    }
}
