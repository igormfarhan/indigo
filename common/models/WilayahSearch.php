<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UbisKecamatan;

/**
 * UbisKecamatanSearch represents the model behind the search form of `common\models\UbisKecamatan`.
 */
class WilayahSearch extends UbisKecamatan
{
    public $witel;
    public $ubis;
    public $kota_kabupaten;
    public $kecamatan;
    public $desa_kelurahan;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['witel','ubis',], 'safe'],
            [['kota_kabupaten', 'kecamatan','desa_kelurahan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$query)
    {
        // $query = UbisKecamatan::find();

        // add conditions that should always apply here
        $query->joinWith('ubis.datel.witel','desaKelurahan','kotaKabupaten');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        
            ]);
        
        $dataProvider->sort->attributes['witel'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['witel.nama' => SORT_ASC],
            'desc' => ['witel.nama' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ubis'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['ubis.nama' => SORT_ASC],
            'desc' => ['ubis.nama' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([

            'witel.id' => $this->witel,
            'ubis.id' => $this->ubis,

        ]);

        // grid filtering conditions
        $query->andFilterWhere(['like', 'ubis_id', $this->ubis_id])
            ->andFilterWhere(['like', 'kecamatan_id', $this->kecamatan_id]);

        return $dataProvider;
    }
}
