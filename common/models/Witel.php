<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "witel".
 *
 * @property string $id
 * @property string|null $nama
 *
 * @property Datel[] $datels
 */
class Witel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'witel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'string', 'max' => 2],
            [['nama'], 'string', 'max' => 128],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * Gets query for [[Datels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUbiskecamatan()
    {
        return $this->hasMany(UbisKecamatan::className(), ['ubis_id' => 'id'])
                    ->via('Ubiss');
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['ubis_id' => 'id'])
                    ->via('Ubiss');
    }

    public function getUbiss()
    {
        return $this->hasMany(Ubis::className(), ['datel_id' => 'id'])
                    ->via('Datels');
    }

    public function getDatels()
    {
        return $this->hasMany(Datel::className(), ['witel_id' => 'id']);
    }


}
