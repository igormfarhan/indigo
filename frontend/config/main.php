<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'session' => [
            'name' => 'FRONTENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
            ],
        ],  
        // 'user' => [
        //     'identityClass' => 'common\models\User',
        //     'enableAutoLogin' => true,
        //     'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        // ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        // 'mail' => [
        //     'class' => 'yii\swiftmailer\Mailer',
        //     'transport' => [
        //         'class' => 'Swift_SmtpTransport',
        //         'host' => 'indi-go.id',
        //         'username' => 'notification@indi-go.id',
        //         'password' => 'Indigojapat1',
        //         'port' => '25',
        //         'encryption' => 'tls',
        //     ],
        // ],
        'reCaptcha3' => [
            'class'      => 'kekaadrenalin\recaptcha3\ReCaptcha',
            'site_key'   => '6LelH7EZAAAAANDKbaY7BLPyBv3S86J2pK13eO1r',
            'secret_key' => '6LelH7EZAAAAAP7LGZq2G4yr0iae5_ovZoJWuyY3',
        ],
        'reCaptcha' => [
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV2' => '6Le3_7EZAAAAAJOkwV3sqVIi2VaeEkN1KLfirq_R',
            'secretV2' => '6Le3_7EZAAAAADY66uy2Xj2n651hyvpVdjnXRMoa',

        ],
    ],
    'params' => $params,
];
