<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Captcha;
use common\models\Pelanggan;
use common\models\Kecamatan;
use common\models\DesaKelurahan;
use common\models\UbisKecamatan;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Pelanggan();
        $captchaModel = new Captcha();

        if ($model->load(Yii::$app->request->post())) {

           
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array(ActiveForm::validate($model));
            }
			
			if($this->isNewCons($model->no_handphone)){
                return $this->redirect(['not-found']);
            }

            $query = UbisKecamatan::find()->asArray()
                                         ->where(['kecamatan_id' => $model->kecamatan_id])
                                         ->all();
            
            $query = ArrayHelper::getColumn($query, 'ubis_id');
            
            $query = User::find() ->where(['ubis_id' => $query])
                                  ->andWhere(['blocked_at' => null,'status'=> 1])
                                  ->orderBy(['order' => SORT_ASC])
                                  ->one();
            // error handling

            $hi = is_null($query);

            if(is_null($query)){
                $model->salesforce_id = null;
				$model->tanggal_submit = time();
                $model->save(false);

                return $this->redirect('/site/not-found');

            }

            $sf = User::findOne($query->id);
            $sf->order += 1;
            $sf->save(false);

            $model->salesforce_id = $query->id;
            $model->tanggal_submit = time();

            $model->save(false);
            // return $this->redirect(['sales', 'id' => $model->id,'sf'=>$sf->id]);      
            

            if ($model->save()) {  
                // return $this->render('index');          
                // return $this->redirect(['sales', 'id' => $model->id,'sf'=>$sf->id]); 
                return $this->render('sales', [
                    'id' => $model->id,
                    'sf' => $sf->id,
                ]);            
            }  else {
                var_dump ($model->getErrors()); 
                die();
            }   
        } else {
            return $this->render('index', [
                'model' => $model,
                'captchaModel' => $captchaModel,
            ]);
            // validation failed: $errors is an array containing error messages
            // $errors = $model->errors;
            // $errors2 = $sf->errors;
        }

    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function actionListkec($id){

        $kecamatans = Kecamatan::find()->where(['kota_kabupaten_id' => $id])
                                       ->orderBy('nama ASC')
                                       ->all();
        // print_r($kecamatans)
				
		if (!empty($kecamatans)) {
            echo "<option>Pilih Kecamatan</option>";
			foreach($kecamatans as $kecamatan) {
				echo "<option value='".$kecamatan->id."'>".$kecamatan->nama."</option>";
			}
		} else {
			echo "<option>-</option>";
		}
    }

    public function actionListdesa($id){

        $desas = DesaKelurahan::find()->where(['kecamatan_id' => $id])
                                       ->orderBy('nama ASC')
                                       ->all();
        // print_r($kecamatans)
				
		if (!empty($desas)) {
			foreach($desas as $desa) {
				echo "<option value='".$desa->id."'>".$desa->nama."</option>";
			}
		} else {
			echo "<option>-</option>";
		}
    }

    public function actionSales($id,$sf)
    {
        return $this->render('sales',[
            'model' => Pelanggan::findOne($id),
            'sf' => User::findOne($sf),
        ]);
    }

    public function actionNotFound(){
        return $this->render('not_found');

    }

    public function actionDeposit(){
        return $this->render('deposit');

    }

    public function actionHome()
    {
        return $this->goHome();
    }

    public function actionEmail()
    {
        Yii::$app->mailer->compose()
                ->setFrom('notification@indi-go.id')
                ->setTo('igormfarhan@gmail.com')
                ->setSubject('Notifikasi')
                ->setHtmlBody('Anda mendapatkan pesanan baru dari Indi-GO')
                ->send();

        return $this->goHome();
    }

    public function isNewCons($nohp)
    {
        $cons = Pelanggan::find()->asArray()->where(['no_handphone'=> $nohp])->all();

        return !empty($cons);
    }
}
