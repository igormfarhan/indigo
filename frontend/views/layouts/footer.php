<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
          <strong>Copyright &copy; <?=date('Y')?> <a href="https://indi-go.id">Indi-GO</a>.</strong> All rights
          reserved.           
          </div>
          <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vesperr-free-bootstrap-template/ -->
            
          </div>
        </div>
        <div class="col-lg-6">
          <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
            <a href="#header" class="scrollto">Home</a>
            <a href="#about" class="scrollto">Tentang Indi-GO</a>
            <!-- <a href="https://indihome.co.id">IndiHome</a>
            <a href="https://telkom.co.id">Telkom</a> -->
          </nav>
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->