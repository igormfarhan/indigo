<?php

use yii\helpers\Url;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

?>

<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">

      <div class="logo mr-auto">
        
        <?php
            $logo = Html::img('@web/img/indigo.png',['alt'=>'IndiHome','class'=>'img-responsive img-fluid']);
            echo Html::a($logo, ['site/index']);
        ?>
        
        <!-- Uncomment below if you prefer to use an image logo -->

        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#header">Home</a></li>
          <li><?=Html::a('Tentang Indi-GO', ['site/index/#about'])?></li>
          <li><?=Html::a('Paket', ['site/index/#paket'])?></li>
          <!-- <li><?=Html::a('FAQ', ['site/index/#faq'])?></li>        -->
          <li class="get-started"><?=Html::a('Hubungi Sales', ['site/index/#call'])?></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
