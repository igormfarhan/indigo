<?php

use yii\helpers\Html;

?>

<!-- ======= About Us Section ======= -->
<section id="about" class="about">
    <div class="container">

    <div class="section-title" data-aos="fade-up">
        <h2>Tentang</h2>
    </div>

    <div class="row content">
        <div class="col-lg-6" data-aos="fade-up" data-aos-delay="150">
        <p>
            Apakah kamu merasa kesulitan ketika hendak ingin pasang layanan Internet dimasa pandemi COVID-19 ini
            karena layanan Offline sementara ditutup? Atau kamu punya masalah di bawah ini?
        </p>
        <ul>
            <li><i class="ri-check-double-line"></i> Kesulitan menentukan paket yang sesuai</li>
            <li><i class="ri-check-double-line"></i> Tidak tahu tentang paket layanan yang tersedia</li>
            <li><i class="ri-check-double-line"></i> Tidak tahu cara mendapatkan layanan Internet???</li>
        </ul>
        </div>
        <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-up" data-aos-delay="300">
        <p>
            Indi-GO merupakan solusi atas permasalah tersebut. Indi-GO merupakan layanan untuk
            mendatangkan Sales ke tempatmu untuk menjawab permasalahan yang sedang kamu hadapi. Sales akan membantumu
            menentukan paket yang sesuai dengan kebutuhanmu dan pemasangan Internet pun menjadi lebih mudah.
        </p>
        <a href="#faq" class="btn-learn-more">F.A.Q</a>
        </div>
    </div>

    </div>
</section><!-- End About Us Section -->

<!-- ======= Counts Section ======= -->
<section id="counts" class="counts">
      <div class="container">

        <div class="row">
          <div class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-xl-start" data-aos="fade-right" data-aos-delay="150">
            <?= Html::img('@web/img/counts-img.svg', ['alt'=>'Jawara', 'class'=>'img-fluid animated']);?>
          </div>

          <div class="col-xl-7 d-flex align-items-stretch pt-4 pt-xl-0" data-aos="fade-left" data-aos-delay="300">
            <div class="content d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-md-6 d-md-flex align-items-md-stretch">
                  <div class="count-box">
                    <i class="icofont-clock-time"></i>
                    <span>Hemat Waktu</span>
                    <p>Cukup melalui web yang dapat diakses melalui smartphone</p>
                  </div>
                </div>

                <div class="col-md-6 d-md-flex align-items-md-stretch">
                  <div class="count-box">
                    <i class="icofont-money"></i>
                    <span>Hemat Biaya</span>
                    <p>Tidak perlu keluar rumah cukup #dirumahaja</p>
                  </div>
                </div>

                <div class="col-md-6 d-md-flex align-items-md-stretch">
                  <div class="count-box">
                    <i class="icofont-users-alt-1"></i>
                    <span>Sales Force</span>
                    <p>Petugas Sales yang selalu siap membantu</p>
                  </div>
                </div>

                <div class="col-md-6 d-md-flex align-items-md-stretch">
                  <div class="count-box">
                    <i class="icofont-world"></i>
                    <span>Jangkauan</span>
                    <p>Jangkauan layanan Indi-GO meliputi seluruh wilayah Jawa Barat</p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
</section><!-- End Counts Section -->