<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\KotaKabupaten;
use common\models\Kecamatan;
use common\models\DesaKelurahan;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model common\models\Pelanggan */
/* @var $form ActiveForm */

$kota = ArrayHelper::map(KotaKabupaten::find()->asArray()->orderBy('nama')->all(), 'id', 'nama');
$kecamatan = ArrayHelper::map(Kecamatan::find()->asArray()->all(), 'id', 'nama');
$desa = ArrayHelper::map(DesaKelurahan::find()->asArray()->all(), 'id', 'nama');
?>


 <!-- ======= Call Section ======= -->
 <section id="call" class="contact">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Call Sales</h2>
        </div>

        <div class="row">

        

          <div class="col-lg-12 col-md-12" data-aos="fade-up" data-aos-delay="300">
          <?php $form = ActiveForm::begin([
          'id' => 'pelanggan-form',
          'fieldConfig' => [
            // 'template' => "{input}\n{hint}\n{error}",
            
          ],
          'errorCssClass' => 'error',
          'enableAjaxValidation' => true,
          'enableClientValidation' => true,
          // 'options' => ['class' => 'php-email-form'],

          ]) ?>

          <?= 
          $form->field($model, 'koordinat')->widget('kolyunya\yii2\widgets\MapInputWidget',[
            'key' => 'AIzaSyDHUEgb0jOwF49OEo4af5DUf8qpXHFmxwU',
            'latitude' => -6.8984729,
            'longitude' => 107.623498,
            'pattern' => '%latitude%,%longitude%',
            'zoom' => 12,
            'height' => '500px',
            'mapType' => 'roadmap',
            'animateMarker' => 1,
            'enableSearchBar' => 1,
            'alignMapCenter' => 1,

          ])->label(false) ?>
         
          
          <?= $form->field($model, 'nama_pelanggan')->label(false)->textInput(['placeholder' => 'Nama Lengkap',]) ?>
          <?= $form->field($model, 'no_handphone')->label(false)->textInput(['placeholder' => 'No. Handphone','maxlength' => true])  ?>
          <?= $form->field($model, 'kota_kabupaten_id')->label(false)->dropDownList($kota,
          [
            'prompt'=>'Kota/Kabupaten Lokasi Pemasangan',
            'id'=>'kota',
            'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl('site/listkec?id=').'"+$(this).val(), function( data ) {
              $( "select#kecamatan" ).html( data );
            });
            '
          ]);   ?>
          <?= $form->field($model, 'kecamatan_id')->label(false)->dropDownList($kecamatan,
          [
            'prompt'=>'Kecamatan Lokasi Pemasangan',
            'id'=>'kecamatan',
            'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl('site/listdesa?id=').'"+$(this).val(), function( data ) {
              $( "select#desa" ).html( data );
            });
            '
          ]);   ?>
          <?= $form->field($model, 'desa_kelurahan_id')->label(false)->dropDownList($desa,
          [
            'prompt'=>'Desa/Kelurahan Lokasi Pemasangan',
            'id'=>'desa',
          ]);   ?>
                  
          <?=
          $form->field($model, 'alamat')->label(false)->textArea(['placeholder' => 'Alamat Pemasangan',])  
          ?>

          <?= $form->field($model, 'status_id')->hiddenInput(['value'=> 101])->label(false) ?>
          <?= $form->field($model, 'hasil_id')->hiddenInput(['value'=> 201])->label(false) ?>
          
          <!-- <?= $form->field($model, 'Captcha')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-2">{image}</div><div class="col-lg-3">{input}</div></div>',
                ])->label(false) ?> -->
          
          <?= $form->field($captchaModel, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha2::className())
          ->label(false) ?>

          </div>

        </div>

        <div class="form-group">
            <?= Html::submitButton('<i class="icofont-user"></i> Hubungi Sales', ['class' => 'btn btn-danger']) ?>
        </div>
        <?php ActiveForm::end(); ?>


      </div>
    </section><!-- End Contact Section -->