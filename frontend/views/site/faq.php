<!-- ======= F.A.Q Section ======= -->
<section id="faq" class="faq">
    <div class="container">

    <div class="section-title" data-aos="fade-up">
        <h2>F.A.Q</h2>
    </div>

    <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
        <div class="col-lg-5">
        <i class="ri-question-line"></i>
            <h4>Apakah saya harus register terlebih dahulu untuk menggunakan layanan IndiPRO?</h4>
        </div>
        <div class="col-lg-7">
            <p>
            Anda bisa secara langsung menggunakan layanan IndiPRO tanpa harus melakukan registrasi
            atau mendaftarkan akun Anda terlebih dahulu.        
            </p>
        </div>
    </div><!-- End F.A.Q Item-->

    <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
        <div class="col-lg-5">
        <i class="ri-question-line"></i>
            <h4>Bagaimana alur untuk melakukan pemasangan IndiHome melalui layanan IndiPRO?</h4>
        </div>
        <div class="col-lg-7">
            <p>
            Klik tombol <a href="#call">Hubungi Sales</a>. Lalu Anda bisa
            menentukan Lokasi Anda berada. Selanjutnya Sales Force terdekat akan menerima permintaan
            Anda. Lalu Sales Force akan langsung menuju lokasi tempat Anda berada.
            </p>
        </div>
    </div><!-- End F.A.Q Item-->

    <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
        <div class="col-lg-5">
        <i class="ri-question-line"></i>
            <h4>Area mana saja yang bisa dijangkau oleh layanan IndiPRO?</h4>
        </div>
        <div class="col-lg-7">
            <p>
            Untuk saat ini layanan IndiPRO hanya bisa melayani permintaan pasang layanan Indihome di
            Area Kota Bandung. Apabila ingin berlangganan IndiHome dari area
            selain yang disebutkan, Anda bisa mengunjungi <span><a href="https://indihome.co.id" target="_blank"> 
            http://indihome.co.id </a></span> atau download
            aplikasinya di Playstore & Appstore        
            </p>
        </div>
    </div><!-- End F.A.Q Item-->

    <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
        <div class="col-lg-5">
        <i class="ri-question-line"></i>
            <h4>Apa saja yang perlu disiapkan untuk melakukan pemasangan IndiHome?</h4>
        </div>
        <div class="col-lg-7">
            <p>
            Anda harus menyiapkan KTP pribadi, data alamat pemasangan, nomor Hp yang bisa dihubungi,
            dan email aktif.        
            </p>
        </div>
    </div><!-- End F.A.Q Item-->

    <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="500">
        <div class="col-lg-5">
        <i class="ri-question-line"></i>
           <h4>Lalu apa langkah selanjutnya?</h4>
        </div>
        <div class="col-lg-7">
            <p>
            Sales Force akan meng-input data Anda untuk
            dilakukan permintaan pasang. Lalu Anda harus bersiap untuk dihubungi oleh Call Center
            Telkom Pusat untuk melakukan konfirmasi. Apabila sudah terkonfirmasi, teknisi akan
            segera melakukan pemasangan IndiHome di tempat Anda.        
            </p>
        </div>
    </div><!-- End F.A.Q Item-->

    <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="500">
        <div class="col-lg-5">
        <i class="ri-question-line"></i>
           <h4>Dimana saya dapat memperoleh informasi tentang paket-paket promo IndiHome yang
            sedang berlaku?</h4>
        </div>
        <div class="col-lg-7">
            <p>
            Anda dapat mengunjugi <span><a href="http://indihome.co.id" target="_blank"> 
            http://indihome.co.id </a></span>untuk melihat paket promo IndiHome yang sedang berlaku
            </p>
        </div>
    </div><!-- End F.A.Q Item-->

    <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="500">
        <div class="col-lg-5">
        <i class="ri-question-line"></i>
           <h4>Apa itu deposit? Dan mengapa saya harus membayar deposit?</h4>
        </div>
        <div class="col-lg-7">
            <p>
            Dalam rangka meningkatkan pelayanan dan demi kenyamanan pelanggan, 
            IndiHome menerapkan sistem jaminan pembayaran untuk pemasangan baru IndiHome.
            Selengkapnya Anda dapat mengunjungi <span>
            <a href="https://www.indihome.co.id/pusat-bantuan/indipedia/detail/Cara+Baru+Pasang+IndiHome+dengan+Jaminan+Pembayaran" target="_blank"> 
            penjelasan deposit </a></span>
            </p>
        </div>
    </div><!-- End F.A.Q Item-->

    </div>
</section><!-- End F.A.Q Section -->