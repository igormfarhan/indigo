<?php

use yii\helpers\Html;

$this->title = 'Indi-GO';

?>

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

<div class="container">
  <div class="row">
  <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
      <h1 data-aos="fade-up">Indi-GO</h1>
       <h1 data-aos="fade-up" style="color:red">Cara Mudah Pasang Internet</h1>
       <h1 data-aos="fade-up" style="color:red">Wilayah Jawa Barat</h1>
       <h5 data-aos="fade-up" data-aos-delay="400">Pasang internet cepat dan stabil</h5>
       <h5 data-aos="fade-up" data-aos-delay="400">dengan menggunakan fiber optic.</h5>
       <h5 data-aos="fade-up" data-aos-delay="400">Ragam pilihan paket internet, telefon, dan TV interaktif</h5>
       <h5 data-aos="fade-up" data-aos-delay="400">dengan bandwidth sesuai kebutuhan Anda.</h5>
      <div data-aos="fade-up" data-aos-delay="800">
        <?= Html::a('Hubungi Sales', ['#call'],['class' => 'btn-get-started scrollto']) ?>
      </div>
    </div>
    <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left" data-aos-delay="200">
       <?= Html::img('@web/img/indira.png', ['alt'=>'Jawara', 'class'=>'img-fluid animated']);?>
    </div>
  </div>
  </div>


</section><!-- End Hero -->

<!-- ======= IndiHome Section ======= -->
<section id="indihome" class="clients clients">
      <div class="container">

        <div class="row">

          <h4>Dapatkan internet broadband, nelpon rumah sepuasnya dan nonton beragam konten terbaik
          di layar TV interaktif hanya di Indi-GO!</h4>

        </div>

      </div>
</section><!-- End IndiHome Section -->

<!-- ======= About Us Section ======= -->
<?= $this->render("about") ?>

<!-- ======= Pricing Section ======= -->
<?= $this->render("paket") ?>

<!-- ======= F.A.Q Section ======= -->
<?php $this->render("faq") ?>

<!-- ======= Call Section ======= -->
<?= $this->render("call", [
  'model' => $model,
  'captchaModel' => $captchaModel,
]) ?>

