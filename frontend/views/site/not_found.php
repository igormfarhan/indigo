<?php

use yii\helpers\Html;

$this->title = 'Not Found ';

?>

<!-- ======= About Us Section ======= -->
<section id="about" class="about">
    <div class="container">
      <div class="section-title" data-aos="fade-up">
      </div>
    </div>
</section><!-- End About Us Section -->

<!-- ======= Counts Section ======= -->
<section id="counts" class="counts">
      <div class="container">

        <div class="row">
          <div class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-xl-start" data-aos="fade-right" data-aos-delay="150">
            <?= Html::img('@web/img/not_found.png', ['alt'=>'Not Found', 'class'=>'img-fluid animated']);?>
          </div>

          <div class="col-xl-7 d-flex align-items-stretch pt-4 pt-xl-0" data-aos="fade-left" data-aos-delay="300">
            <div class="content d-flex flex-column justify-content-center">
              <div class="row">
                
                
                
                <h2>Data kamu sudah terekam.</h2>
                <h2>Kami akan segera menghubungimu.</h2>
                <h4>Follow kami aja dulu di </br>
                <?= Html::a('<i class="icofont-instagram"></i> Instagram', 'https://instagram.com/telkomjabar', ['class' => 'btn btn-danger']) ?>
                </h4>


                
                <!-- <h1>Mohon maaf layanan ini belum tersedia di daerah kamu</h1>
                <h2>Tapi tenang saja, kami akan segera menghubungimu</h2> -->
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
</section><!-- End Counts Section -->