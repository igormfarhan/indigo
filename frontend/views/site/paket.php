<!-- ======= Pricing Section ======= -->
<section id="paket" class="pricing">
  <div class="container">

    <div class="section-title">
      <h2>Paket</h2>
      <p>Beragam pilihan paket Layanan sesuai dengan kebutuhan kamu</p>
    </div>

    <div class="row">

      <div class="col-lg-3 col-md-6">
        <div class="box" data-aos="zoom-in-right" data-aos-delay="200">
          <h3>Paket A</h3>
          <h4><sup>Rp</sup>280.000<span>/bulan</span></h4>
          <ul>
            <li><i class="icofont-speed-meter"></i>10-100 Mbps</li>
            <li><i class="icofont-ui-dial-phone"></i>Bebas 300 menit Telfon</li>
            <li><i class="icofont-cloudapp"></i>Bebas 8GB Cloud Storage</li>
            <li><i class="icofont-read-book"></i>Bebas Akses Layanan Study</li>
            <li><i class="icofont-music"></i>Bebas Akses Layanan Music</li>
          </ul>
          <div class="btn-wrap">
            <a href="#" class="btn-buy">Pilih Paket</a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
        <div class="box" data-aos="zoom-in-left" data-aos-delay="200">
        <h3>Paket B</h3>
          <h4><sup>Rp</sup>320.000<span>/bulan</span></h4>
          <ul>
            <li><i class="icofont-speed-meter"></i>10-100 Mbps</li>
            <li><i class="icofont-contrast"></i>93 Channel Interaktif</li>
            <li><i class="icofont-ui-dial-phone"></i>Bebas 300 menit Telfon</li>
            <li><i class="icofont-read-book"></i>Bebas Akses Layanan Study/</li>
            <li><i class="icofont-music"></i>Bebas Akses Layanan Music</li>
            
          </ul>
          <div class="btn-wrap">
            <a href="#call" class="btn-buy">Pilih Paket</a>
          </div>
        </div>
      </div>

      
      <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
        <div class="box recommended" data-aos="zoom-in" data-aos-delay="100">
        <h3>Paket C</h3>
          <h4><sup>Rp</sup>470.000<span>/bulan</span></h4>
          <ul>
            <li><i class="icofont-speed-meter"></i>10-100 Mbps</li>
            <li><i class="icofont-contrast"></i>162 Channel Interaktif</li>
            <li><i class="icofont-ui-dial-phone"></i>Bebas 1000 menit Telfon</li>
            <li><i class="icofont-read-book"></i>Bebas Akses Layanan Study/</li>
            <li><i class="icofont-music"></i>Bebas Akses Layanan Music</li>            
          </ul>
          <div class="btn-wrap">
            <a href="#call" class="btn-buy">Pilih Paket</a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
        <div class="box" data-aos="zoom-in-left" data-aos-delay="200">
        <h3>Paket D</h3>
          <h4><sup>Rp</sup>380.000<span>/bulan</span></h4>
          <ul>
            <li><i class="icofont-speed-meter"></i>10-100 Mbps</li>
            <li><i class="icofont-contrast"></i>93 Channel Interaktif</li>
            <li><i class="icofont-ui-dial-phone"></i>Bebas 100 menit Telfon</li>
            <li><i class="icofont-game-pad"></i>Bonus Item Eksklusif</li>
            <li class="na"><i class="icofont-music"></i>Bebas Akses Layanan Music</li>            
          </ul>
          <div class="btn-wrap">
            <a href="#call" class="btn-buy">Pilih Paket</a>
          </div>
        </div>
      </div>

    </div>

    <div class="row" style="padding-top:10px">
    <div class="col-lg-12 col-md-12 mt-12 mt-lg-0">
        <div class="box" data-aos="zoom-in-left" data-aos-delay="200">
          
          <!-- <div class="btn-wrap">
            <a href="https://www.indihome.co.id/" target="_blank" class="btn-buy">Pilihan Paket Lainnya</a>
          </div> -->
        </div>
      </div>
    <div>

  </div>
</section><!-- End Pricing Section -->