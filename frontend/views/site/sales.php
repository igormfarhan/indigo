<?php

use yii\helpers\Html;
use common\models\Pelanggan;
use common\models\User;

$this->title = 'Indi-GO';

$model = Pelanggan::findOne($id);
$sf = User::findOne($sf);

$wa = 'https://wa.me/';
$wa .= preg_replace('/[^\p{L}\p{N}\s]/u', '', $sf->no_handphone);
$wa .= '?text=';
$wa .= '*Pesan dari Indi-GO*. ';
$text = '%0D%0A'.'Hallo, saya ingin memasang IndiHome.';
$text .= '%0D%0A'. 'Nama : '. $model->nama_pelanggan;
$text .= '%0D%0A'. 'No. HP : '. $model->no_handphone;
$text .= '%0D%0A'. 'Kota/Kabupaten : '. $model->kotaKabupaten->nama;
$text .= '%0D%0A'. 'Kecamatan : '. $model->kecamatan->nama;
$text .= '%0D%0A'. 'Desa/Kelurahan : '. $model->desaKelurahan->nama;
$text .= '%0D%0A'. 'Alamat : '. $model->alamat;

$email = 'Anda Mendapatkan Pesanan baru dari Indi-GO';
$email .= '<br> Harap segera follow up dan update status';
$email .= '<br> Nama : '. $model->nama_pelanggan;
$email .= '<br> No. HP : '. $model->no_handphone;
$email .= '<br> Kota/Kabupaten : '. $model->kotaKabupaten->nama;
$email .= '<br> Kecamatan : '. $model->kecamatan->nama;
$email .= '<br> Desa/Kelurahan : '. $model->desaKelurahan->nama;
$email .= '<br> Alamat : '. $model->alamat;

Yii::$app->mailer->compose()
                ->setFrom('notification@indi-go.id')
                ->setTo($sf->email)
                ->setSubject('Notifikasi')
                ->setHtmlBody($email)
                ->send();

?>

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center" style="padding-top:0px">

<div class="container team section-bg">
      <h1 class="text-center">Silahkan Hubungi Sales</h1>
        <div class="row">
        
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch offset-lg-4"
          style="margin-top:30px">
            <div class="member" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
              <?= Html::img('@web/dashboard/uploads/avatar/'.$sf->avatar, 
                            ['alt' => 'avatar','class'=>'img-fluid']) 
                        ?>
                
              </div>
              <div class="member-info">
                 <?= Html::tag('h4', Html::encode($sf->nama)) ?>
                 <?= Html::tag('h6','NIK : '. Html::encode($sf->nik)) ?>
                 <?= Html::a('<i class="icofont-whatsapp"></i> Whatsapp',
                            $wa . $text, 
                            ['class' => 'btn btn-success']) 
                 ?>
                 <?= Html::a('<i class="ri-phone-line"></i> Telefon',
                            'tel:'.$sf->no_handphone, 
                            ['class' => 'btn btn-primary']) 
                 ?>
                 <?= Html::a('<i class="ri-mail-send-line"></i> SMS',
                            'sms:'.$sf->no_handphone, 
                            ['class' => 'btn btn-info']) 
                 ?>
              </div>
            </div>
          </div>

        </div>

</div>
</section><!-- End Hero -->