<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dektrium\user\models;

use dektrium\user\helpers\Password;
use dektrium\user\Mailer;
use dektrium\user\Module;
use dektrium\user\traits\ModuleTrait;
use Yii;
use yii\base\Model;


/**
 * SettingsForm gets user's nik, email and password and changes them.
 *
 * @property User $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SettingsForm extends Model
{
    use ModuleTrait;

    /** @var string */
    public $email;

    /** @var string */
    public $nik;

    /** @var string */
    public $nama;

    /** @var string */
    public $no_handphone;

    /** @var string */
    public $ubis_id;

    /** @var string */
    public $new_password;

    /** @var string */
    public $verify_password;

    /** @var string */
    public $current_password;
    
    /** @var string */
    public $avatar;

    /** @var string */
    public $status;

    /** @var Mailer */
    protected $mailer;

    /** @var User */
    private $_user;

    /** @return User */
    public function getUser()
    {
        if ($this->_user == null) {
            $this->_user = Yii::$app->user->identity;
        }

        return $this->_user;
    }

    /** @inheritdoc */
    public function __construct(Mailer $mailer, $config = [])
    {
        $this->mailer = $mailer;
        $this->setAttributes([
            'nik' => $this->user->nik,
            'nama' => $this->user->nama,
            'ubis_id' => $this->user->ubis_id,
            'no_handphone' => $this->user->no_handphone,
            'email'    => $this->user->unconfirmed_email ?: $this->user->email,
            'status' => $this->user->status,
        ], false);
        parent::__construct($config);
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            'no_handphoneTrim' => ['no_handphone', 'trim'],
            'no_handphoneLength'   => ['no_handphone', 'required', 'message'=>'No Handphone harus diisi'],
            'no_handphonePattern'   => ['no_handphone', 'match', 'pattern' => '/^[6]*[2]*[8]/', 
                                        'message'=> 'Harus dimulai dengan 62 lalu 8, contoh: 62812'],
            'no_handphoneLength'   => ['no_handphone', 'string', 'min' => 9, 'max' => 15],
            
            'statusLength'   => ['status', 'integer'],
            'avatarType'   => ['avatar', 'string'],
            'avatarRequired'   => ['avatar', 'required'],
            'nikTrim' => ['nik', 'trim'],
            'namaRequired' => ['nama', 'required'],
            'nikRequired' => ['nik', 'required'],
            'nikLength'   => ['nik', 'string', 'min' => 3, 'max' => 255],
            'nikPattern' => ['nik', 'match', 'pattern' => '/^[-a-zA-Z0-9_\.@]+$/'],
            'emailTrim' => ['email', 'trim'],
            'emailRequired' => ['email', 'required'],
            'emailPattern' => ['email', 'email'],
            'emailnikUnique' => [['email', 'nik'], 'unique', 'when' => function ($model, $attribute) {
                return $this->user->$attribute != $model->$attribute;
            }, 'targetClass' => $this->module->modelMap['User']],
            'newPasswordLength' => ['new_password', 'string', 'max' => 72, 'min' => 6],
            'verifyPasswordLength' => ['verify_password', 'string', 'max' => 72, 'min' => 6],
            'verifyPasswordRequired' => ['verify_password', 'required','on'=>'verify'],
            'currentPasswordRequired' => ['current_password', 'required'],
            'currentPasswordValidate' => ['current_password', function ($attr) {
                if (!Password::validate($this->$attr, $this->user->password_hash)) {
                    $this->addError($attr, 'Password lama salah');
                }
            }],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'email'            => Yii::t('user', 'Email'),
            'nik'              => Yii::t('user', 'NIK'),
            'nama'             => 'Nama Lengkap', 
            'ubis_id'             => 'Ubis', 
            'no_handphone'             => 'No Handphone', 
            'new_password'     => Yii::t('user', 'Password Baru'),
            'verify_password'     => Yii::t('user', 'Password Baru'),
            'current_password' => Yii::t('user', 'Password Lama'),
            'status' => Yii::t('user', 'Status'),
        ];
    }

    /** @inheritdoc */
    public function formName()
    {
        return 'settings-form';
    }

    /**
     * Saves new account settings.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $this->user->scenario = 'settings';
            $this->user->nik = $this->nik;
            $this->user->nama = strtoupper($this->nama);
            $this->user->no_handphone = $this->no_handphone;
            $this->user->ubis_id = $this->ubis_id;
            $this->user->status = $this->status;
            $this->user->avatar = $this->avatar;
            $this->user->password = $this->new_password;
            $this->user->password = $this->verify_password;
            if ($this->email == $this->user->email && $this->user->unconfirmed_email != null) {
                $this->user->unconfirmed_email = null;
            } elseif ($this->user->email == null){
                $this->user->email = $this->email;
            } 
            elseif ($this->email != $this->user->email) {
                switch ($this->module->emailChangeStrategy) {
                    case Module::STRATEGY_INSECURE:
                        $this->insecureEmailChange();
                        break;
                    case Module::STRATEGY_DEFAULT:
                        $this->defaultEmailChange();
                        break;
                    case Module::STRATEGY_SECURE:
                        $this->secureEmailChange();
                        break;
                    default:
                        throw new \OutOfBoundsException('Invalid email changing strategy');
                }
            }

            return $this->user->save();
        }

        return false;
    }

    /**
     * Changes user's email address to given without any confirmation.
     */
    protected function insecureEmailChange()
    {
        $this->user->email = $this->email;
        Yii::$app->session->setFlash('success', Yii::t('user', 'Your email address has been changed'));
    }

    /**
     * Sends a confirmation message to user's email address with link to confirm changing of email.
     */
    protected function defaultEmailChange()
    {
        $this->user->unconfirmed_email = $this->email;
        /** @var Token $token */
        $token = Yii::createObject([
            'class'   => Token::className(),
            'user_id' => $this->user->id,
            'type'    => Token::TYPE_CONFIRM_NEW_EMAIL,
        ]);
        $token->save(false);
        $this->mailer->sendReconfirmationMessage($this->user, $token);
        Yii::$app->session->setFlash(
            'info',
            Yii::t('user', 'A confirmation message has been sent to your new email address')
        );
    }

    /**
     * Sends a confirmation message to both old and new email addresses with link to confirm changing of email.
     *
     * @throws \yii\base\InvalidConfigException
     */
    protected function secureEmailChange()
    {
        $this->defaultEmailChange();
        /** @var Token $token */
        $token = Yii::createObject([
            'class'   => Token::className(),
            'user_id' => $this->user->id,
            'type'    => Token::TYPE_CONFIRM_OLD_EMAIL,
        ]);
        $token->save(false);
        $this->mailer->sendReconfirmationMessage($this->user, $token);

        // unset flags if they exist
        $this->user->flags &= ~User::NEW_EMAIL_CONFIRMED;
        $this->user->flags &= ~User::OLD_EMAIL_CONFIRMED;
        $this->user->save(false);

        Yii::$app->session->setFlash(
            'info',
            Yii::t(
                'user',
                'We have sent confirmation links to both old and new email addresses. You must click both links to complete your request'
            )
        );
    }
}
