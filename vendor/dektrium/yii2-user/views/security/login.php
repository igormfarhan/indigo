<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use aryelds\sweetalert\SweetAlert;

$session = Yii::$app->session->getAllFlashes();

foreach (Yii::$app->session->getAllFlashes() as $message) {
    echo SweetAlert::widget([
        'options' => [
            'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
            // 'text' => (!empty($message['text'])) ? Html::encode($message['text']) : 'Text Not Set!',
            'type' => (!empty($message['type'])) ? $message['type'] : SweetAlert::TYPE_INFO,
            // 'timer' => (!empty($message['timer'])) ? $message['timer'] : 4000,
            'showConfirmButton' =>  (!empty($message['showConfirmButton'])) ? $message['showConfirmButton'] : true
        ]
    ]);
}


/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>",
    'inputOptions' => [
        'autofocus' => 'autofocus',
        'class' => 'form-control',
        'tabindex' => '1'],
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>",
    'inputOptions' => [
        'autofocus' => 'autofocus',
        'class' => 'form-control',
        'tabindex' => '2'],
];

?>

<?php
    $text = "Bagi yang sudah pernah update data lalu lupa password";
    $text .= " Silahkan gunakan fitur Lupa Password";
    $text .= " atau silahkan hubungi KaUbis masing-masing";
    if(!array_key_exists('success',$session)){
        echo SweetAlert::widget([
            'options' => [
                'title' => "PENGUMUMAN!",
                'text' => $text,
                'type' => SweetAlert::TYPE_INFO,
            ]
        ]);
    }
    
?>

<div class="login-box">    
    <div class="login-box-body">
        <div class="text-center" style="padding-top:15px">
            <?= Html::img('@web/img/indigo.png', ['alt'=>'Indigo', 'style'=>'width: 200px; margin-bottom: 15px']);?>
        </div>
        <h4 class="login-box-msg">Masuk</h4>


        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?php if ($module->debug): ?>
            <?= $form->field($model, 'login',$fieldOptions1)
                     ->label(false)
                     ->textInput(['placeholder' => $model->getAttributeLabel('Nomor Induk Karyawan')])
                     ->dropDownList(LoginForm::loginList());
            ?>

        <?php else: ?>

            <?= $form->field($model, 'login',$fieldOptions1)->label('ID Partner')?>

        <?php endif ?>

        <?php if ($module->debug): ?>
            <div class="alert alert-warning">
                <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
            </div>

        <?php else: ?>
            <?= $form->field($model,'password',$fieldOptions2)
                ->passwordInput()
                ->label(
                    Yii::t('user', 'Password')
                      . ($module->enablePasswordRecovery ?
                       ' (' . Html::a(
                           Yii::t('user', 'Lupa Password?'),
                           ['/user/recovery/request'],
                           ['tabindex' => '5']
                       )
                       . ')' : '')
                ) ?>
        <?php endif ?>
        

        <div style="padding-top:10px">

        <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '3']) ?>

        <?= Html::submitButton('Masuk',
            ['class' => 'btn btn-primary btn-block','name' => 'login-button', 'tabindex' => '4']) 
        ?>
        <?php ActiveForm::end(); ?>   

        </div>

        <div style="padding-top:20px">  
		
		<?= Html::a('IT Helpdesk', 'https://t.me/indigo_helpdesk',) ?>

         <!--  <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Belum terima email konfirmasi?'), ['/user/registration/resend']) ?>
            </p>
        <?php endif ?> -->
        <!-- <?php if ($module->enableRegistration): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Belum punya akun? Daftar di sini!'), ['/user/registration/register']) ?>
            </p>
        <?php endif ?> -->
        <?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>     

        </div>
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
