<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use developit\jcrop\Jcrop;
use yii\helpers\Url;
use common\models\Ubis;
use common\models\Witel;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\SettingsForm $model
 */

$this->title = Yii::t('user', 'Account settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <!-- <?= $this->render('_menu') ?> -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title text-center">Account detail</h3>
          </div>
              
          <div class="box-body box-profile">
            <?= Html::img('@web/uploads/avatar/'.Yii::$app->User->identity->avatar, 
                ['alt' => 'avatar','class'=>'profile-user-img img-responsive',
                'style'=> 'width:400px']) 
            ?>

            <h3 class="profile-username text-center"><?= Yii::$app->User->identity->nama ?></h3>
            
            <p class="text-muted text-center"></p>

            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Homebase</b> <a class="pull-right">
                <?php
                    if (\Yii::$app->user->can('user-witel')) {
                       echo  Witel::find()->where(['id'=>Yii::$app->user->identity->ubis_id])->one()->nama;
                    } elseif (\Yii::$app->user->can('user-ubis')) {
                       echo Ubis::find()->where(['id'=>Yii::$app->user->identity->ubis_id])->one()->nama;
                    } elseif (\Yii::$app->user->can('salesforce')) {
                        echo Ubis::find()->where(['id'=>Yii::$app->user->identity->ubis_id])->one()->nama;
                     } ?>
                    </a>
              </li>
              <li class="list-group-item">
                <b>Jumlah Hit</b> <a class="pull-right"><?= Yii::$app->user->identity->order ?></a>
              </li>
              <li class="list-group-item">
                <b>Terakhir login</b> <a class="pull-right"><?= Yii::$app->formatter->asDate(Yii::$app->user->identity->last_login_at) ?></a>
              </li>
              <li class="list-group-item">
                <b>Terakhir update</b> <a class="pull-right"><?= Yii::$app->formatter->asDate(Yii::$app->user->identity->updated_at) ?></a>
              </li>
            </ul>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin([
                    'id' => 'account-form',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>

                <!-- <div id="input" class="col-sm-offset-3 col-lg-9 text-center">
                    <?//= Html::img('@web/uploads/avatar/'.Yii::$app->User->identity->avatar, ['alt' => 'My logo']) ?>
                </div> -->

                
                <?= $form->field($model, 'avatar')->widget(Jcrop::className(), [
                'uploadUrl' => Url::toRoute('/salesforce/avatar'),
            ]) ?>

                <?= $form->field($model, 'avatar')
                    ->hiddenInput(['value'=> Yii::$app->user->identity->nik . '_avatar.jpg'])
                    ->label(false)
                ?>


                <?= $form->field($model, 'nik')->textInput(['disabled' => true]) ?>

                <?= $form->field($model, 'nama')?>

                <?= $form->field($model, 'no_handphone')->textInput(['placeholder' => 'Harus dimulai dengan 62'])
                ?>

                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])
                ?>               

                <?= $form->field($model, 'new_password')->passwordInput() ?>

                <?php
                // echo $form->field($model, 'avatar')->widget(FileInput::classname(), [
                //       'options' => ['accept' => 'image/*'],
                //       'pluginOptions'=>[
                //                         'allowedFileExtensions'=>['jpeg','jpg','gif','png'],
                //                         'showUpload' => false,
                //                         'showPreview' => true,
                //                         'showCaption' => true,
                //                         'showRemove' => true,
                //                        ],
                //     ]);   
                ?>

                <hr/>

                <?= $form->field($model, 'current_password')->passwordInput() ?>

                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-primary']) ?><br>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

        <?php if ($model->module->enableAccountDelete): ?>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('user', 'Delete account') ?></h3>
                </div>
                <div class="panel-body">
                    <p>
                        <?= Yii::t('user', 'Once you delete your account, there is no going back') ?>.
                        <?= Yii::t('user', 'It will be deleted forever') ?>.
                        <?= Yii::t('user', 'Please be certain') ?>.
                    </p>
                    <?= Html::a(Yii::t('user', 'Delete account'), ['delete'], [
                        'class' => 'btn btn-danger',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure? There is no going back'),
                    ]) ?>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>

