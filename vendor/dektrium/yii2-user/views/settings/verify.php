<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use developit\jcrop\Jcrop;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\SettingsForm $model
 */

$this->title = Yii::t('user', 'Account settings');
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user->identity;

$fieldOptions1 = [
    'options' => ['class' => 'form-group input-group'],
    'template' => "<span class='input-group-addon'>+62</span>{input}"
];
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>


<div class="login-box">    
    <div class="login-box-body">
        <div class="text-center" style="padding-top:15px">
            <?= Html::img('@web/img/indigo.png', ['alt'=>'Jawara', 'style'=>'width: 200px; margin-bottom: 15px']);?>
        </div>
        <h4 class="login-box-msg">Silahkan ubah password dan upload Foto</h4>


        <?php $form = ActiveForm::begin([
                    'id' => 'account-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>

        <?= $form->field($model, 'avatar')->widget(Jcrop::className(), [
                'uploadUrl' => Url::toRoute('/salesforce/avatar'),
            ])->label(false) ?>

        <?= $form->field($model, 'nik')
                 ->label(false)
                 ->textInput(['disabled' => true])
        ?>
        <?= $form->field($model, 'nama')
                 ->label(false)
        ?>

        <?= $form->field($model, 'no_handphone')
                 ->label(false)
                 ->textInput(['placeholder' => 'Nomor HP dimulai dengan 62'])
        ?>

        <?= $form->field($model, 'email')
                 ->label(false)
                 ->textInput(['placeholder' => 'Email'])
        ?>
        

        <?= $form->field($model, 'verify_password')
                 ->passwordInput(['placeholder' => 'Password Baru'])
                 ->label(false) ?>
        
        <hr/>

        <?= $form->field($model, 'current_password')
                 ->passwordInput(['placeholder' => 'Password Lama'])
                 ->label(false) ?> 

        <div style="padding-top:10px">

        <?= Html::submitButton('Simpan',
            ['class' => 'btn btn-primary btn-block','name' => 'login-button', 'tabindex' => '4']) 
        ?>
        <?php ActiveForm::end(); ?>   

        </div>


    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
