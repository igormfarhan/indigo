<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.13.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-bootstrap4' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap4',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/bootstrap4' => $vendorDir . '/yiisoft/yii2-bootstrap4/src',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'dmstr/yii2-adminlte-asset' => 
  array (
    'name' => 'dmstr/yii2-adminlte-asset',
    'version' => '2.6.2.0',
    'alias' => 
    array (
      '@dmstr' => $vendorDir . '/dmstr/yii2-adminlte-asset',
    ),
  ),
  'kolyunya/yii2-map-input-widget' => 
  array (
    'name' => 'kolyunya/yii2-map-input-widget',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kolyunya/yii2' => $vendorDir . '/kolyunya/yii2-map-input-widget/sources',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.12.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient/src',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.2.7.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient/src',
    ),
  ),
  'dektrium/yii2-user' => 
  array (
    'name' => 'dektrium/yii2-user',
    'version' => '0.9.14.0',
    'alias' => 
    array (
      '@dektrium/user' => $vendorDir . '/dektrium/yii2-user',
    ),
    'bootstrap' => 'dektrium\\user\\Bootstrap',
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-sortable' => 
  array (
    'name' => 'kartik-v/yii2-sortable',
    'version' => '1.2.2.0',
    'alias' => 
    array (
      '@kartik/sortable' => $vendorDir . '/kartik-v/yii2-sortable/src',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.1.7.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.5.8.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform/src',
    ),
  ),
  'kartik-v/yii2-bootstrap4-dropdown' => 
  array (
    'name' => 'kartik-v/yii2-bootstrap4-dropdown',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/bs4dropdown' => $vendorDir . '/kartik-v/yii2-bootstrap4-dropdown/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.3.5.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'kartik-v/yii2-dynagrid' => 
  array (
    'name' => 'kartik-v/yii2-dynagrid',
    'version' => '1.5.1.0',
    'alias' => 
    array (
      '@kartik/dynagrid' => $vendorDir . '/kartik-v/yii2-dynagrid/src',
    ),
  ),
  'kartik-v/yii2-export' => 
  array (
    'name' => 'kartik-v/yii2-export',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/export' => $vendorDir . '/kartik-v/yii2-export/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.2.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine/src',
    ),
  ),
  'developit/yii2-jcrop' => 
  array (
    'name' => 'developit/yii2-jcrop',
    'version' => '1.1.4.0',
    'alias' => 
    array (
      '@developit/jcrop' => $vendorDir . '/developit/yii2-jcrop/src',
    ),
  ),
  'mdmsoft/yii2-admin' => 
  array (
    'name' => 'mdmsoft/yii2-admin',
    'version' => '2.9.0.0',
    'alias' => 
    array (
      '@mdm/admin' => $vendorDir . '/mdmsoft/yii2-admin',
    ),
  ),
  'aryelds/yii2-sweet-alert' => 
  array (
    'name' => 'aryelds/yii2-sweet-alert',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@aryelds/sweetalert' => $vendorDir . '/aryelds/yii2-sweet-alert',
    ),
  ),
  'yii2mod/yii2-sweet-alert' => 
  array (
    'name' => 'yii2mod/yii2-sweet-alert',
    'version' => '1.3.0.0',
    'alias' => 
    array (
      '@yii2mod/alert' => $vendorDir . '/yii2mod/yii2-sweet-alert',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker/src',
    ),
  ),
  'kartik-v/yii2-helpers' => 
  array (
    'name' => 'kartik-v/yii2-helpers',
    'version' => '1.3.9.0',
    'alias' => 
    array (
      '@kartik/helpers' => $vendorDir . '/kartik-v/yii2-helpers/src',
    ),
  ),
  'kartik-v/yii2-field-range' => 
  array (
    'name' => 'kartik-v/yii2-field-range',
    'version' => '1.3.5.0',
    'alias' => 
    array (
      '@kartik/field' => $vendorDir . '/kartik-v/yii2-field-range/src',
    ),
  ),
  'kekaadrenalin/yii2-module-recaptcha-v3' => 
  array (
    'name' => 'kekaadrenalin/yii2-module-recaptcha-v3',
    'version' => '1.4.0.0',
    'alias' => 
    array (
      '@kekaadrenalin/recaptcha3' => $vendorDir . '/kekaadrenalin/yii2-module-recaptcha-v3/src',
    ),
  ),
  'himiklab/yii2-recaptcha-widget' => 
  array (
    'name' => 'himiklab/yii2-recaptcha-widget',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@himiklab/yii2/recaptcha' => $vendorDir . '/himiklab/yii2-recaptcha-widget/src',
      '@himiklab/yii2/recaptcha/tests' => $vendorDir . '/himiklab/yii2-recaptcha-widget/tests',
    ),
  ),
);
